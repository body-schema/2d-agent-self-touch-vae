%% Datasets for denoising VAE

% learning is done on self-touch configurations
% find self-touch configuration (here any touch is self-touch)
touch_indexes= find(sum(S,2)>0); %touch indexes
% touch_indexes=touch_indexes(1:3000);
Ntouch=length(touch_indexes); %number of touches

MS=[M_coded(touch_indexes,:),S(touch_indexes,:)]; %input data (concatenation of proprioception and tactile)

% add noise for Denoising VAE
snr1=10;
MSwhitenoise=min(max(awgn(MS,snr1),0),1);
%MSwhitenoise2=min(max(awgn(MS,5),0),1);

% corrupted and target datasets
MS_full=[MS;MSwhitenoise]; % corrupted dataset (include noisy inputs and original inputs)
MS_target=repmat(MS,size(MS_full,1)/size(MS,1),1); % target dataset (inputs to be reconstructed by the VAE)

%% prepare data
% random permutation of data 
Ntotal=size(MS_full,1); %total size
trainingtesting_ratio=.8;%amount of training
idx=randperm(Ntotal); %random permutation of data 
idxTrain=idx(1:round(trainingtesting_ratio*Ntotal)); %training indexes
idxTest=idx(round(trainingtesting_ratio*Ntotal)+1:end); %testing indexes

% build the training and testing datasets
XTrain_noise  = MS_full(idxTrain,:);
XTrain_target = MS_target(idxTrain,:);

XTest_noise  = MS_full(idxTest,:);
XTest_target = MS_target(idxTest,:);

% Transform data  into Deep learning array for custom learning
XTest_noise_dl = dlarray(single(XTest_noise'), 'CB');  % C for channel dimension, B for batch dimension
XTest_target_dl= dlarray(single(XTest_target'), 'CB');