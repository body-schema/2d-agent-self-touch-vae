function [infGrad, genGrad] = modelGradients_denoising_bVAE(encoderNet, decoderNet, xNoise, xTarget, varargin)
[z, zMean, zLogvar] = sampling(encoderNet, xNoise);
xPred = sigmoid(forward(decoderNet, z));
loss = ELBOloss(xTarget, xPred, zMean, zLogvar, varargin{1});
[genGrad, infGrad] = dlgradient(loss, decoderNet.Learnables, ...
    encoderNet.Learnables);
end