function elbo = ELBOloss(x, xPred, zMean, zLogvar,varargin)
% squares = 0.5*(xPred-x).^2;
% %reconstructionLoss = mse(xPred,x);
% reconstructionLoss  = sum(squares, [1,2,3]);
% % reconstructionLoss  = sum(squares, [1]);
% KL = mean(-.5 * sum(1 + zLogvar - zMean.^2 - exp(zLogvar), 1));
% %elbo = mean(reconstructionLoss + 2 * KL);
% elbo = mean(reconstructionLoss + KL * varargin{1});


reconstructionLoss = mse(xPred,x);
KL = mean(-.5 * sum(1 + zLogvar - zMean.^2 - exp(zLogvar), 1));
elbo = reconstructionLoss + KL* varargin{1};
end