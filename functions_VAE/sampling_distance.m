function [zSampled, zMean, zLogvar] = sampling_distance(encoderNet, x , distance)

compressed = forward(encoderNet, x);
d = size(compressed,1)/2;
zMean = compressed(1:d,:);
zLogvar = compressed(1+d:end,:);

sz = size(zMean);
epsilon = randn(sz);
sigma = exp(.5 * zLogvar) * distance;
z = epsilon .* sigma + zMean;
%z = reshape(z, [1,1,sz]);
zSampled = dlarray(z, 'CB');
end