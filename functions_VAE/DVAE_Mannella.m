%% VAE architecture
inputSize = [size(MS,2)];
latentDim = 3; 
visualization = 1;
%% deep layer sizes
dim3=latentDim *2*2;
dim2=round(dim3*2.5);
dim1=round(dim2*3);

%% define networks
encoderLG = layerGraph([featureInputLayer(inputSize,'Name','input_encoder','Normalization','none')'
    fullyConnectedLayer(dim1, 'Name', 'fc_encoder0')
    reluLayer('Name','relu1')
    fullyConnectedLayer(dim2, 'Name', 'fc_encoder1')
    reluLayer('Name','relu2')
    fullyConnectedLayer(dim3, 'Name', 'fc_encoder2')
    reluLayer('Name','relu3')
    fullyConnectedLayer(2 * latentDim, 'Name', 'fc_encoder3')
    %reluLayer('Name','relu4')
    ]);

decoderLG = layerGraph([
    featureInputLayer(latentDim,'Name','ii','Normalization','none')
    fullyConnectedLayer(dim3, 'Name', 'fc_decoder0')
    reluLayer('Name','relu5')
    fullyConnectedLayer(dim2, 'Name', 'fc_encoder1')
    reluLayer('Name','relu6')
    fullyConnectedLayer(dim1, 'Name', 'fc_encoder2')
    reluLayer('Name','relu7')
    fullyConnectedLayer(inputSize, 'Name', 'fc_decoder_out')
    %clippedReluLayer(1,'Name','relu8')
    ]);

%names
encoderNet = dlnetwork(encoderLG);
decoderNet = dlnetwork(decoderLG);

%% for use of gpu or cpu
%executionEnvironment = 'auto';
executionEnvironment = "none";
%% Training

numEpochs= 2000;%number of training epochs
miniBatchSize = 400; %mini batch size
lr = 1e-5; %learning rate
beta0 = .1; %beta parameter for weighting the elbo loss of a beta-autoencoder (if beta=1 -> traditionnal VAE )
%beta =logspace(-1,0,numEpochs);
%beta = [0*ones(1,30),1*ones(1,numEpochs-30)];
beta=beta0*ones(1,numEpochs); %to change dynamically the beta parameter during training (cf. papers)
numIterations = floor(trainingtesting_ratio*Ntotal/miniBatchSize); %number of training iterations

%% init
%init gradients
avgGradientsEncoder = [];
avgGradientsSquaredEncoder = [];
avgGradientsDecoder = [];
avgGradientsSquaredDecoder = [];

%init error vectors
elbo_i=[];
rec_err_epoch_proprio=[];
rec_err_epoch_sensory=[];
ee=[]; 

%init counters
iteration = 0;
cnt=0;
%% go
for epoch = 1:numEpochs
    tic;
    for i = 1:numIterations
        iteration = iteration + 1;
        %waitbar2(iteration,numIterations,10)
        
        %compute train and target batches 
        idx = (i-1)*miniBatchSize+1:i*miniBatchSize;
        XBatch_noise = XTrain_noise(idx,:)';
        XBatch_target = XTrain_target(idx,:)';

        XBatch_noise = dlarray(single(XBatch_noise), 'CB');
        XBatch_target = dlarray(single(XBatch_target), 'CB');
        
        %gpu or cpu
        if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
            XBatch_noise = gpuArray(XBatch_noise);  
            XBatch_target = gpuArray(XBatch_target);      
        end 
        
        %compute gradients
        [infGrad, genGrad] = dlfeval(...
            @modelGradients_denoising_bVAE, encoderNet, decoderNet, XBatch_noise, XBatch_target,beta(epoch));
        %[infGrad, genGrad] = dlfeval(...
        %    @modelGradients_denoising, encoderNet, decoderNet, XBatch_noise, XBatch_target);
        %compute decoder updates with adam optim
        [decoderNet.Learnables, avgGradientsDecoder, avgGradientsSquaredDecoder] = ...
            adamupdate(decoderNet.Learnables, ...
                genGrad, avgGradientsDecoder, avgGradientsSquaredDecoder, iteration, lr);
        %compute encoder updates
        [encoderNet.Learnables, avgGradientsEncoder, avgGradientsSquaredEncoder] = ...
            adamupdate(encoderNet.Learnables, ...
                infGrad, avgGradientsEncoder, avgGradientsSquaredEncoder, iteration, lr);
    end
    elapsedTime = toc;
    
    % compute loss
    [z, zMean, zLogvar] = sampling(encoderNet, XTest_noise_dl);
 

    xPred = sigmoid(forward(decoderNet, z));
    elbo = ELBOloss(XTest_target_dl, xPred, zMean, zLogvar,beta(epoch));
    %rec_err_epoch(epoch) = ELBOloss(XTest_target_dl, xPred, zMean, zLogvar,0);
    
    %compute reconstruction error for proprio and touch
    rec_err_epoch_proprio(epoch)=(mean(sqrt(sum((xPred(1:size(M_coded,2),:)-XTest_target_dl(1:size(M_coded,2),:)).^2,1))))/(size(M_coded,2));%mean error
    rec_err_epoch_sensory(epoch)=(mean(sqrt(sum((xPred(size(M_coded,2)+1:end,:)-XTest_target_dl(size(M_coded,2)+1:end,:)).^2,1)))/body.tactilesensors.nb);%mean error
    
    %follow errors
    elbo_i(epoch)=elbo;
    if visualization == 1
        if ~mod(epoch,20)
            %%
            figure(1)
            
            clf
            subplot(133)
            zl=extractdata(zLogvar);
            hist(zl',100)
            leg={};
            for ii=1:size(zl,1)
                leg{ii}=['$log \sigma',sprintf('_%d^2',ii),'$'];
            end
            legend(leg,'Interpreter','latex','Location','Northwest')
            title('Logvar for each latent dimension')
            set(gca,'fontsize',16)
            
            %plot(elbo_i,'-')
            %hold on
            subplot(131)
            plot((rec_err_epoch_proprio),'-')
            hold on
            plot((rec_err_epoch_sensory),'-')
            xlabel('epochs')
            ylabel('reconstruction error')
            legend({'proprio reconstruction','tactile reconstruction'})
            set(gca,'fontsize',16)
            subplot(132)
            plot(mean(abs(extractdata(XTest_noise_dl-xPred)),2))
            xlabel('epochs')
            ylabel('reconstruction error')
            set(gca,'fontsize',16)
            title('for each sensory input')
            if ~mod(epoch,20)&&latentDim==3
                %% show some low dimensional projection of data points
                sensibility=.9;
                % no touch
                inotouch=find(sum(S,2)==0);
                MSnt=[M_coded(inotouch(1:end),:),S(inotouch(1:end),:)];
                MSnt_dl=dlarray(single(MSnt'),'CB');
                [~, zmnt, zLogvar] = sampling(encoderNet, MSnt_dl);
                zMnt=extractdata(zmnt);
                % touches with sensors
                zMi={};MSi_dl={};
                figure(2)
                clf
                plott(zMnt(:,1:10:end),'k.') %this is non touch points
                hold on
                for i=1:10:size(MS,2) %% show the projection when 10 choosend sensors are activated above a threshold.
                    ii=find(MS(:,i)>sensibility);
                    if ~isempty(ii)
                        MSi=MS(ii,:);%[M_coded(ii,:),S(ii,:)];
                        MSi_dl{i}=dlarray(single(MSi'),'CB');
                        [~, zmi, zLogvar] = sampling(encoderNet, MSi_dl{i});
                        zMi{i}=extractdata(zmi);
                        plott(zMi{i}(1:min(size(zLogvar,1),3),:),'.')%;i*ones(1,size(zMi{i},2))],'.')
                    end
                    
                end
                
                axis equal
                view(3)
                title('projection of data point to VAE')
            end
            drawnow
        end
        
    end
    
    disp("Epoch : "+epoch+" Test ELBO loss = "+gather(extractdata(elbo))+...
        ". Time taken for epoch = "+ elapsedTime + "s")
    
end
%%