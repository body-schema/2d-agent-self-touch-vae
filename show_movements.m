%% A script to show movements of the agent
close all
figure(1)
clf
grid off

for n = 1:min(N,100)
    [Px,Py]=get_agent_limbs(M(n,:),body);
    
    %plot_mannella(Px,Py,sensors,S(n,:),1);
    plot_circle_body(Px,Py,body.tactilesensors.means,round(S(n,:)*100)+10,'k','linewidth',4)
    
    hold off
    title(sprintf('frame %d',n))
    axis equal
    axis([-3 3 -1 2])

    pause(.05)
    %         drawnow
end