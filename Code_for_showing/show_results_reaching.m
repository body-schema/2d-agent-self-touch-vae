%% images reaching
close all
%% start position
p0=[0.6223    1.7068    1.9631    1.2978    0.4697    1.7735]; %very
p0=[0.1,0.1,0.1,0.1,0.1,0.1]; % initial proprioception
%p0=[0.9464    1.3000    1.8    0.5142    0.3972    0.1136]
%p0=body.motor.motor_constraints_min;
p=p0;
p_hist=p;
% target position 
x=1.8; %easy 3.8
s_target0=mannella_artificial_touch_continuous_position(x,body);
%s_target0=eye(10);
s_target0=s_target0(:)';
%s_target0=zeros(1,size(S,2));
s_target=s_target0;

%%  
figure(2)
clf
plot_touch(p,x,body,500,'y','k','linewidth',4);
hold on
plot_agent(p,s_target0,body,'m','k','linewidth',4);
axis equal

%%
close all
goal_mask=[zeros(1,size(M_coded,2)),s_target]>0.1;
goal_mask=[zeros(1,size(M_coded,2)),ones(1,size(S,2))]>0;
%goal_mask=[zeros(1,size(M_coded,2)),s_target]>0.1;
goal_mask=[ones(1,size(M_coded,2)),zeros(1,size(S,2))]>0;
%noise_mask=[ones(1,body.propriosensors.nb/2),zeros(1,body.propriosensors.nb/2),ones(1,body.tactilesensors.nb)]<.01;

% criterion (a1 use of self reconstruction error, a2 use of current target
% reconstruction)
    %% control param
dpmax=0.2;dpnoise=0.0;
dp_inertia=0; %inertia from last dp
windowstime=50;nz=10;sampling_spread=2;
%% init
pcoded_history=[];
ptarget_history=[];
err_rec_history=[];
errmin_rec_history=[];
err_goal_history=[];
err_rec_current_history=[];
err_rec_current_history_goal=[];
err_rec_actual_notsimul=[];
criterion_history=[];
diffp_hist=[];
XTouch_hist={};
dp_old=zeros(1,6);
clear ps_pred
%%
figure(1)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for n=1:windowstime
    n
    %%
    p_coded = compute_activation(p,body);
    %%
    [Px,Py]=get_agent_limbs(p,body);
    [sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
    
    ps_real=[p_coded,sensory_real];
    %% FROM TARGET SENSATION COMPUTE TARGET MS
    s_target_real=max(s_target,sensory_real);
    
    %s_target_real=s_target;
    ps_target_current=[p_coded,s_target_real];
    
    %ps_target_current(noise_mask)=0;
    
    ps_target_current_dl=dlarray(ps_target_current','CB');


    err_rec_target=[];
    err_conf=[];
    subplot(221)
    hold off
    tic
    for j=1:nz
        if 0%j==1&&n>1
            ps_pred(:,j)=pcoded_reconstructed;
        else
            [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
            zm=z;
            ps_pred(:,j) = sigmoid(forward(decoderNet, zm));
        end
        pcoded_reconstructed = (ps_pred(:,j))';
%         %% motor error
%         p_desired=pcoded_reconstructed(1:size(M_coded,2));
%         
%         p_reconstructed=net(extractdata((p_desired))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
%         p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
%         
%         
%         p_desired_decoded = compute_activation(p_reconstructed,body);
%         
%         dp=mse(p_desired_decoded,extractdata(p_desired));
        %% compute reconstruction error:
        
        err_rec_target(j)=mean((ps_target_current(goal_mask)-pcoded_reconstructed(goal_mask)).^2);
%         err_rec_target(j)=dp+mean((ps_target_current(goal_mask)-pcoded_reconstructed(goal_mask)).^2);

        %err_rec_target(j)=mean((ps_target_current-pcoded_reconstructed).^2);

        %%
        p_reconstructed=net((extractdata(pcoded_reconstructed(1:size(M_coded,2)))'))'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
        p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
        
        s_reconstructed=extractdata(pcoded_reconstructed(size(M_coded,2)+1:end));
        %%
        if j<10
            plot_agent(p_reconstructed,s_reconstructed,body,'w','k','linewidth',1);
            hold on
        end
    end
    
    title('Actual and generated self-touch configurations','fontsize',20)
    %%
    criterion=err_rec_target;
    %%
    pcoded_reconstructed=(ps_pred(:,find(criterion==min(criterion),1)))';
    
    %%
    p_reconstructed=net((extractdata(pcoded_reconstructed(1:size(M_coded,2))))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
    p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
    s_reconstructed=extractdata(pcoded_reconstructed(size(M_coded,2)+1:end));

    plot_agent(p_reconstructed,s_reconstructed,body,'r','r','linewidth',2);
    hold on
    %change objective touched
    
    pcoded_history(n,:)=pcoded_reconstructed;
    ptarget_history(n,:)=ps_target_current;
    errmin_rec_history(n)=min(err_rec_target);
    err_rec_history(n,:)=(err_rec_target);
    fprintf('Error goal simulated: %2.4f\n', min(err_rec_target));
    
    
    sensoryactivation_desired=pcoded_reconstructed(:,size(p_coded,2)+1:end);
    %     %compute desired proprio angles
    p_desired=pcoded_reconstructed(1:size(M_coded,2));
    
    p_reconstructed=net(extractdata((p_desired))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
    p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
    
    
    p_desired_decoded = compute_activation(p_reconstructed,body);
    
    diffp_hist(n)=mse(p_desired_decoded,extractdata(p_desired));
    %% different kind of commands   
    % %saturation (go in direction but cannot go furhter than dpmax
    dp=min(abs(p_reconstructed-p)+(rand(1,6)-1/2)*2*dpnoise,dpmax).*sign(p_reconstructed-p);
    % %speed command (goes in the direction with a factor 0.3
    %dp=(p_reconstructed-p)*0.3;
    % %speed with inertia takes into account previous speed
    %dp=(p_reconstructed-p)*0.2+dp_old*0.05;
    %show
    
    subplot(221)
    %%
    plot_touch(p,x,body,500,'y','k','linewidth',4);
    hold on
    plot_agent(p,s_target0,body,'m','k','linewidth',4);
    axis([-2.5,2.5,-.01,2])
    axis equal
    
    subplot(212)
    hold off
    stem(s_target_real,'linewidth',2)
    hold on
    plot(extractdata(sensoryactivation_desired),'r.-','linewidth',2)
    stem(abs(extractdata(sensoryactivation_desired)-s_target_real),'linewidth',4)
    legend({'Actual touch','Generated touch','surprise'})
        title('Actual touch and generated touch activations','fontsize',20)

    subplot(222)
    hold off
    
    
    %plot(criterion_history,'.-','markersize',20)
    %plot(err_rec_history,'.-','markersize',20)
    %plot(criterion_history,'.-','markersize',20)
    semilogy(err_rec_history(:,1:j),'k.','markersize',20)
    hold on
    semilogy(errmin_rec_history,'r.-','markersize',20)
    
    %plot(diffp_hist,'b.-','markersize',20)
    title('Self-reaching criterion','fontsize',20)

    pause(.001)
    %%
    if min(errmin_rec_history)<3e-3
        'finished'
        break
    end
      %  
    %p=constmot(p+dp+dp_old*dp_inertia,m_const_min,m_const_max); %next pose
    p=max(min(p+dp+dp_old*dp_inertia,body.motor.motor_constraints_max),body.motor.motor_constraints_min);
    p_hist(end+1,:)=p;
    
    dp_old=dp;
    
end

%% show trajectories:
figure(1)
clf
% plot firstextractdata
[WP,Px,Py,O]=moverobrot(body.arm_lengths(2:end),p_hist(1,:));
Px=[-body.arm_lengths(1),0,Px];Py=[0,0,Py];
[ss2,ii]=mannella_tactile_continuous(Px,Py,body);
% plot_mannella(Px,Py,sensors,s_target0,1)
hold on
plot_mannella(Px,Py,body,zeros(1,size(S,2)),'c','linewidth',4);
hold on
xtouch_local=[Px(n_limb),Py(n_limb)]+xper*[Px(n_limb+1)-Px(n_limb),Py(n_limb+1)-Py(n_limb)];
hold on
xxx_new=plot_mannella_poseintersection(Px,Py,xtouch_local','ko','markersize',20,'markerfacecolor','y','linewidth',2);

%plot last
[WP,Px,Py,O]=moverobrot(body.arm_lengths(2:end),p_hist(end,:));
Px=[-body.arm_lengths(1),0,Px];Py=[0,0,Py];
[ss2,ii,X]=mannella_tactile_continuous(Px,Py,sensors);
% plot_mannella(Px,Py,sensors,ss2,1)
hold on
plot_mannella(Px,Py,sensors,zeros(1,size(S,2)),1,'b','linewidth',4);
hold on
xxx_new=plot_mannella_poseintersection(Px,Py,X,'ko','markersize',10,'markerfacecolor','r','linewidth',2);

axis tight
axis equal

axis off
%%
c0=[0.9,0.9,0.9]*1.1;
for i=2:size(p_hist,1)-1
    plot_agent(p_hist(i,:),zeros(1,size(S,2)),body,'r','color',c0*(1-i/size(p_hist,1)),'linewidth',2);
    hold on
    drawnow
end
hold on
plot_touch(p0,x,body,500,'y','c','linewidth',4);
%%
figure(2)
clf
semilogy(errmin_rec_history,'.-','markersize',20,'linewidth',4)
grid on
legend({'Goal reconstruction error','Current reconstruction error'})
title('Reconstruction error')
%%
