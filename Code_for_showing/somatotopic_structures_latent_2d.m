%% 2d show of latent space
sensibility=.99;
% no touch
inotouch=find(sum(S,2)==0);
MSnt=[M_coded(inotouch(1:end),:),S(inotouch(1:end),:)];
MSnt_dl=dlarray(single(MSnt'),'CB');
[~, zmnt, zLogvar] = sampling(encoderNet, MSnt_dl);
zMnt=extractdata(zmnt);
% touch
itouch=find(sum(S,2)>1);
MSt=[M_coded(itouch(1:end),:),S(itouch(1:end),:)];
MSt_dl=dlarray(single(MSt'),'CB');
[~, zmt, zLogvar] = sampling(encoderNet, MSt_dl);
zMt=extractdata(zmt);
% touches with sensors
zMi={};MSi_dl={};
for i=1:size(MS,2)
    i
   ii=find(MS(:,i)>sensibility);
    MSi=MS(ii,:);%[M_coded(ii,:),S(ii,:)];
    MSi_dl{i}=dlarray(single(MSi'),'CB');
end
%%
for i=1:size(MS,2)
    [~, zmi, zLogvar] = sampling(encoderNet, MSi_dl{i});
    zMi{i}=extractdata(zmi);
end
%%
figure(3)
cm=jet(size(MS,2)-size(M_coded,2));
clf
plott(zMnt(:,1:10:end),'k.')
hold on
for i=size(M_coded,2)+1:size(MS,2)
    %plott([zMi{i}(1:2,:);i*ones(1,size(zMi{i},2))],'.')
    scatter3(zMi{i}(1,:),zMi{i}(2,:),zMi{i}(3,:),10,cm(i-size(M_coded,2),:),'filled')
    drawnow
    %pause(.1)
end
xlabel('z_1')
ylabel('z_2')
zlabel('z_3')
set(gca,'fontsize',16)
axis square
%% latent left torso right
figure(3)
cm=jet(size(MS,2)-size(M_coded,2));
leftarm=1:38;rightarm=62:100;
clf
h=plott(zMnt(:,1:10:end),'k.')
hold on
for i=size(M_coded,2)+leftarm(1):size(M_coded,2)+leftarm(end)
    %plott([zMi{i}(1:2,:);i*ones(1,size(zMi{i},2))],'.')
    if i==size(M_coded,2)+leftarm(1)
    s1=scatter3(zMi{i}(1,:),zMi{i}(2,:),zMi{i}(3,:),20,'b');
    else
        scatter3(zMi{i}(1,:),zMi{i}(2,:),zMi{i}(3,:),20,'b')
    end
    
    drawnow
    %pause(.1)
end
for i=size(M_coded,2)+rightarm(1):size(M_coded,2)+rightarm(end)
    %plott([zMi{i}(1:2,:);i*ones(1,size(zMi{i},2))],'.')
    if i==size(M_coded,2)+rightarm(1)
    s2=scatter3(zMi{i}(1,:),zMi{i}(2,:),zMi{i}(3,:),20,'r','+');
    else
        scatter3(zMi{i}(1,:),zMi{i}(2,:),zMi{i}(3,:),20,'r','+')
    end
    drawnow
    %pause(.1)
end
xlabel('z_1')
ylabel('z_2')
zlabel('z_3')
legend([h,s1,s2],{'non-touch config','left hand','right hand'},'Location','best')
set(gca,'fontsize',16)
axis square
title('Latent code with activation above .99')
%%

[~, zMean, zLogvar] = sampling(encoderNet, dlarray(MS', 'CB'));

zm=extractdata(zMean);
%%
video=1;
datestr=cellstr(datetime('now','Format', 'yy-MM-dd-HH-mm'));
if video
 writerObj = VideoWriter(['/home/abdelnasser/PostDoc/Datasets/VAE_Mannela_2/somatotopy_torso_latentspace_',datestr{:},'.avi']);
 writerObj.FrameRate = 5;
  open(writerObj);
end
%%
figure(2)

i1=50+size(M_coded,2);
%C1=corr(S(touch_indexes,1),S(touch_indexes,:));
for i2=size(M_coded,2)+1:220
Si1=MS(:,i1);
Si2=MS(:,i2);
%
scatter3(zm(1,:),zm(2,:),zm(3,:),20,(Si1.*Si2),'filled')%[Si1,zeros(size(Si1)),Si2])
view(90,0)
title(sprintf('coactivation tactile sensor %d with tactile sensor %d',i1-size(M_coded,2),i2-size(M_coded,2)))
    colorbar
caxis([0,1])
xlabel('z_1')
ylabel('z_2')
zlabel('z_3')

drawnow
if video
    writeVideo(writerObj, getframe(gcf));
    end
end
if video
    close(writerObj);
end
%scatter3(zm(1,:),zm(2,:),zm(3,:),10,[Si2,zeros(size(Si1,1),2)].*Si1)