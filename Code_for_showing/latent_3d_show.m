%% parameters
sensory_sensibility=.1;
criterion_base=4e-3;
goal_mask=[ones(1,size(M_coded,2)),ones(1,size(S,2))]>0;

%% control param
dpmax=.1 ;dpnoise=0.;
dp_inertia=0; %inertia from last dp
windowstime=50;nz=100;sampling_spread=5;
%% display?
displ=0;
%% start position
p0=[0.6223    1.7068    1.1631    0.2978    0.4697    1.7735]; %very
%
p0=[0.1,0.1,0.1,0.5,0.5,0.5];
%p0=[0.9464    1.3000    1.8    0.5142    0.3972    0.1136]
%p0=body.motor.motor_constraints_min;

p_hist=p0;

%% target positions
x=5;
convergence_criterion=[];
n_convergence=[];
p_convergence=[];
crit_convergence=[];
ds_end=[];

fprintf('external touch @ %2.2f\n',x)
% compute target sensations
s_target0=mannella_artificial_touch_continuous_position(x,body);
%goal_mask=[zeros(1,size(M_coded,2)),s_target0]>0.1;
%%
convergence_criterion=criterion_base;



%% init
pcoded_history=[];
ptarget_history=[];
err_rec_history=[];
errmin_rec_history=[];
err_goal_history=[];
err_rec_current_history=[];
err_rec_current_history_goal=[];
err_rec_actual_notsimul=[];
dp_old=zeros(1,6);
zmean_hist=[];
%%
clf
p=p0;
crr=[];
for n=1:windowstime
    %%
    p_coded = compute_activation(p,body);
    %%
    [Px,Py]=get_agent_limbs(p,body);
    [sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
    ps_real=[p_coded,sensory_real];
    %% FROM TARGET SENSATION COMPUTE TARGET MS
    ds_hist(n)= mse(s_target0(s_target0>sensory_sensibility),sensory_real(s_target0>sensory_sensibility));
    
    s_target_real=max(s_target0,sensory_real);
    
    ps_target_current=[p_coded,s_target_real];
    
    ps_target_current_dl=dlarray(ps_target_current','CB');
    
    err_rec_target=[];
    err_conf=[];
    ps_pred=[];
    if displ
        figure(2)
        hold off
    end
    
     [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
     zmean_hist(n,:)=extractdata(zMean);
    %%
    err_rec_target=[];
    z_rec=[];
    for j=1:nz
        %% sample
        [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
        zm=z;
        z_rec(:,j)=z;
        ps_pred(:,j) = sigmoid(forward(decoderNet, zm));
        
        %% compute reconstruction error:
        err_rec_target(j)=mse(ps_target_current(goal_mask),ps_pred(goal_mask,j)');
    end
    criterion=err_rec_target;
    crr(n)=min(criterion);
    %%
    
    %%
    pcoded_reconstructed=(ps_pred(:,find(criterion==min(criterion),1)))';
    s_rec=pcoded_reconstructed(size(MS,2)+1:end);
    s_hist(n,:)=s_rec;
    %% compute desired joint angles
    p_reconstructed=net(pcoded_reconstructed(1:size(M_coded,2))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
    p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
    
    %% compute command (multiple possibilities)
    % %saturation (go in direction but cannot go furhter than dpmax
    dp=min(abs(p_reconstructed-p)+(rand(1,6)-1/2)*2*dpnoise,dpmax).*sign(p_reconstructed-p);
    % %speed command (goes in the direction with a factor 0.3
    %dp=(p_reconstructed-p)*0.3;
    % %speed with inertia takes into account previous speed
    %dp=(p_reconstructed-p)*0.3+dp_old*0.1;
    %%
    if 1
        %%
 
        subplot(121)
        hold off
        
        hold on
        p_hist(n,:)=p;
        
        plot_agent(p,s_rec,body,'m','k','linewidth',4);
        hold on
        plot_touch(p,x,body,500,'y','k','linewidth',4);
        axis([-3,2,-.0,2])
        axis equal
        axis off
        subplot(122)
        hold off
%         for i=61:size(MS,2)
%             scatter3(zMi{i}(1,:),zMi{i}(2,:),zMi{i}(3,:),1,'k')
%             hold on
%         end
        scatter3(extractdata(zmt(1,:)),extractdata(zmt(2,:)),extractdata(zmt(3,:)),1,'k')
%         toplot1=intersect(zMi{find(s_target0==max(s_target0))}',zMi{1}','rows')';
%         toplotend=intersect(zMi{find(s_target0==max(s_target0))}',zMi{end}','rows')';
%         plot3(toplot1(1,:),toplot1(2,:),toplot1(3,:),'c.')
%          plot3(toplotend(1,:),toplotend(2,:),toplotend(3,:),'m.')
        
        hold on
          plot3(zmean_hist(1:n,1),zmean_hist(1:n,2),zmean_hist(1:n,3),'g-','linewidth',2)
          scatter3(zmean_hist(1:n,1),zmean_hist(1:n,2),zmean_hist(1:n,3),50,1-[n/40,1,n/40],'filled')
          %plot3((z_rec(1==min(criterion))),(z_rec(2,criterion==min(criterion))),(z_rec(3,criterion==min(criterion))),'g.','markersize',20);
          axis square
          view(98,3.1)
          %caxis([convergence_criterion .1])
          %colorbar
%           c=colorbar;
%           set(c,'Limits',[convergence_criterion .1])
% axis equal
%           axis([-3,3,-3,3,-3,3])
          %axis tight
        %             plot(ps_target_current(goal_mask))
        %             hold on
        %             plot(pcoded_reconstructed(goal_mask))
%         
% figure(2)
% clf
% plot(crr,'.k-','markersize',20)
%         hold on
%         %plot(squeeze(ds_hist(rep,1:n)),'.r-','markersize',20)
        drawnow
        %
        
        
        %hold on
        %plot(.5*exp(extractdata(zLogvar)))
        
    end
    %%
    if min(criterion)<convergence_criterion
        fprintf('finished @ n=%d, final dist sensory %2.2f\n',n,ds_hist(n))
        break
    elseif n==windowstime
        fprintf('not converged crit=%2.4f, min dist sensory %2.2f\n',min(criterion),ds_hist(n))
    end
    %
    %p=constmot(p+dp+dp_old*dp_inertia,m_const_min,m_const_max); %next pose
    p=max(min(p+dp+dp_old*dp_inertia,body.motor.motor_constraints_max),body.motor.motor_constraints_min);
    dp_old=dp;
end
%%


subplot(122)
hold off
scatter3(extractdata(zmt(1,:)),extractdata(zmt(2,:)),extractdata(zmt(3,:)),1,'k')
hold on
plot3(zmean_hist(1:n,1),zmean_hist(1:n,2),zmean_hist(1:n,3),'g-','linewidth',2)
scatter3(zmean_hist(1:n,1),zmean_hist(1:n,2),zmean_hist(1:n,3),200,(1:n)/n,'filled')
colormap('autumn')
view(98,3.1)
axis square
xlabel('z_1')
ylabel('z_2')
zlabel('z_3')
set(gca,'Fontsize',18)
    %%
j=jet;
    subplot(221)
    hold off
for k=1:1:n
    k

        
        
        plot_agent(p_hist(k,:),zeros(1,size(S,2)),body,'w','k','linewidth',4);
        hold on
        [Px,Py]=get_agent_limbs(p_hist(k,:),body);
        plot_star_body(Px,Py,x,500,k,'k','linewidth',2)
        %plot_touch(p_hist(k,:),x,body,500,'y','k','linewidth',4);
        drawnow
end
axis equal
axis([-3,2,-0.01,2])
        
        axis off

%%
subplot(223)
semilogy(crr,'.k-','markersize',20)
hold on
scatter(1:n,crr,60,1:n,'filled')
hold on
line([0,40],[criterion_base,criterion_base],'color','r','linestyle','--')
axis([0,40,1e-3,.1])
yticks([1e-3,criterion_base,1e-2,1e-1])
yticklabels({'1e-3','\epsilon','1e-2','1e-1'})
set(gca,'Fontsize',18)