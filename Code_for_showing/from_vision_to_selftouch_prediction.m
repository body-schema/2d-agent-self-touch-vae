%% from vision to self-touch prediction
%generate figures
from_index=10;
%% find vision activation that generates small self-touch errors
%random sampling in latent vision space from self-seeing examples
mv_bezproprio=MV;

mv_bezproprio(:,1:body.nb_motors*body.propriosensors.nb)=0;
%for each zm_vision generate proprio
reconstruction_error_sample=[];
for kk=1:20
    kk
[xPred,vv,latent_samples, latent_mean, latent_logvar]=VAE_pass(encoderNet_vision,decoderNet_vision,mv_bezproprio,1,1);
[vv,xPred,latent_samples, latent_vision_mean, latent_logvar]=VAE_pass(encoderNet_vision,decoderNet_vision,xPred,1,1);

%
p_self_vision_decoded=xPred(:,1:body.nb_motors*body.propriosensors.nb-body.nb_motors_camera*body.propriosensors.nb); %take arm proprio
%do VAE pass on touch, with zeros touch
zerotouch=[p_self_vision_decoded,zeros(size(p_self_vision_decoded,1),body.tactilesensors.nb)];
[ouput_samples,output_mean,latent_samples, latent_mean, latent_logvar]=VAE_pass(encoderNet,decoderNet,zerotouch,1,1);
[ouput_samples,output_mean,latent_samples, latent_mean, latent_logvar]=VAE_pass(encoderNet,decoderNet,ouput_samples,1,1);
%ouput_samples=squeeze(ouput_samples);
norm_var=mean(exp(latent_logvar).^2,1);
% compute self-touch reconstruction error on proprio
proprio_in=p_self_vision_decoded;
proprio_out=output_mean(:,1:body.nb_motors*body.propriosensors.nb-body.nb_motors_camera*body.propriosensors.nb);

reconstruction_error_sample(:,kk)=sqrt(mean((proprio_out-proprio_in).^2,2));
end
reconstruction_error=mean(reconstruction_error_sample,2);
%% find smaller reconstruction error
[a,i_sort]=sort(reconstruction_error);
%% show configuration from self-vision that led to smaller self-touch proprio reconstruction error
affichage_row=4;
affichage_col=10;
clf
cnt=0;
for i=i_sort(1:affichage_row*affichage_col)'
    cnt=cnt+1;
    subplot(affichage_row*2,affichage_col,cnt)
    imagesc(reshape(V(vision_indexes(i),:),body.camera.nb_pixel_x,body.camera.nb_pixel_y))
    
end

% show configurations from self-vision with maximal self-touch error
for i=i_sort(end-affichage_row*affichage_col+1:end)'
    cnt=cnt+1;
    subplot(affichage_row*2,affichage_col,cnt)
    imagesc(reshape(V(vision_indexes(i),:),body.camera.nb_pixel_x,body.camera.nb_pixel_y))
    
end
%% show in latent space
MV_dl = dlarray(single(mv_bezproprio'), 'CB');
[~,zm_vision, zLogvar] = sampling(encoderNet_vision, MV_dl);
figure
zm_vision_ex=extractdata(zm_vision)';
subplot(121)
scatter3(zm_vision_ex(:,1),zm_vision_ex(:,2),zm_vision_ex(:,3),10,reconstruction_error,'filled')
xlabel('z_1'),ylabel('z_2'),zlabel('z_3')
subplot(122)
scatter3(zm_vision_ex(:,1),zm_vision_ex(:,4),zm_vision_ex(:,5),10,reconstruction_error,'filled')
xlabel('z_1'),ylabel('z_4'),zlabel('z_5')
title('Visual latent space, with self-touch proprio reconstruction error, based on vision only')
%% imagesc for showing
i=i_sort(10000);
reconstruction_error(i)
figure(1) %original vision
clf
imagesc(reshape(V(vision_indexes(i),:),body.camera.nb_pixel_x,body.camera.nb_pixel_y)')
axis xy
axis square
%
figure(2) %original configuration
clf
subplot(311)
plot_agent_vision(M(vision_indexes(i),:),body,V(vision_indexes(i),:))

title('Original configuration')
subplot(312) %reconstructed configuration from vision only
proprio=proprio_decoding(xPred(i,1:body.nb_motors*body.propriosensors.nb),net_include_neck,body);
plot_agent_vision(proprio,body,V(vision_indexes(i),:))
title('reconstructed proprio from vision')
%
subplot(313) %reconstructed reconstructed proprio from touch
proprio_rectouch=proprio_decoding(proprio_out(i,:),net,body);
touch_rec=output_mean(i,body.nb_motors_arms*body.propriosensors.nb+1:end);
plot_agent(proprio_rectouch(1:body.nb_motors_arms),touch_rec,body,'m','k','linewidth',4);
axis equal
axis([-2,2,-.5,2])
title('reconstructed touch proprio from reconstructio proprio from vision')
