%% parameters
sensory_sensibility=.1;
criterion_base=5e-3;
goal_mask=[ones(1,size(M_coded,2)),ones(1,size(S,2))]>0;

%% control param
dpmax=.3;dpnoise=0.;
dp_inertia=0; %inertia from last dp
windowstime=50;nz=20;sampling_spread=1;
%% display?
displ=0;
%% start position
p0=[0.6223    1.7068    1.1631    0.2978    0.4697    1.7735]; %very
%
p0=[0.1,0.1,0.1,0.1,0.1,0.1];
%p0=[0.9464    1.3000    1.8    0.5142    0.3972    0.1136]
%p0=body.motor.motor_constraints_min;

p_hist=p0;

%% target positions
x=1.1    ;
convergence_criterion=[];
n_convergence=[];
p_convergence=[];
crit_convergence=[];
ds_hist_i=NaN*zeros(length(xvec),Nrep,windowstime);
ds_end=[];

fprintf('external touch @ %2.2f\n',x)
% compute target sensations
s_target0=mannella_artificial_touch_continuous_position(x,body);
%goal_mask=[zeros(1,size(M_coded,2)),s_target0]>0.1;
%%
convergence_criterion=criterion_base;
ds_hist=NaN*zeros(Nrep,windowstime);



%% init
pcoded_history=[];
ptarget_history=[];
err_rec_history=[];
errmin_rec_history=[];
err_goal_history=[];
err_rec_current_history=[];
err_rec_current_history_goal=[];
err_rec_actual_notsimul=[];
dp_old=zeros(1,6);
zmean_hist=[];
%%
clf
p=p0;
crr=[];
for n=1:windowstime
    %%
    p_coded = compute_activation(p,body);
    %%
    [Px,Py]=get_agent_limbs(p,body);
    [sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
    ps_real=[p_coded,sensory_real];
    %% FROM TARGET SENSATION COMPUTE TARGET MS
    ds_hist(rep,n)= mse(s_target0(s_target0>sensory_sensibility),sensory_real(s_target0>sensory_sensibility));
    
    s_target_real=max(s_target0,sensory_real);
    
    ps_target_current=[p_coded,s_target_real];
    
    ps_target_current_dl=dlarray(ps_target_current','CB');
    
    err_rec_target=[];
    err_conf=[];
    ps_pred=[];
    if displ
        figure(2)
        hold off
    end
    
     [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
     zmean_hist(n,:)=extractdata(zMean);
    ngrid=10;
    sd=2;
    z1grid=linspace(zMean(1)-sd*exp(.5*zLogvar(1)),zMean(1)+sd*exp(.5*zLogvar(1)),ngrid);
    z2grid=linspace(zMean(2)-sd*exp(.5*zLogvar(2)),zMean(2)+sd*exp(.5*zLogvar(2)),ngrid);
    [zzgrid1,zzgrid2]=meshgrid(z1grid,z2grid);
    z1=zzgrid1(:);z2=zzgrid2(:);
   err_grid=[];diffp_hist=[];
    for j=1:length(z1)
        waitbar2(j,length(z1),10)
        %% sample
        %zm=[z1(j),z2(j)]';
        zm=dlarray([z1(j),z2(j)]','CB');
        
        ps_pred(:,j) = sigmoid(forward(decoderNet, zm));
        
        %for proprio reconstruction error
%         p_desired=ps_pred(1:size(M_coded,2),j)';
%         
%         p_reconstructed=net(p_desired')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
%         p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
%         
%         
%         p_desired_decoded = compute_activation(p_reconstructed,body);
        
        %diffp_hist(j)=mse(p_desired_decoded',(p_desired)');
        %% compute reconstruction error:
        err_grid(j)=mean((ps_target_current(goal_mask)-ps_pred(goal_mask,j)').^2);
        
        
        if displ
            
            ez=extractdata(z);
            plot(extractdata(zMean(1)),extractdata(zMean(2)),'r+','markersize',10)
            hold on
            plot(ez(1),ez(2),'k.','markersize',round(-log(err_grid(j))*20))
            axis equal
            axis([-3,3,-1,5])
            
        end
    end
    
    %%
    err_rec_target=[];
    z_rec=[];
    for j=1:nz
        %% sample
        [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
        zm=z;
        z_rec(:,j)=z;
        ps_pred(:,j) = sigmoid(forward(decoderNet, zm));
        
        %% compute reconstruction error:
        err_rec_target(j)=mean((ps_target_current(goal_mask)-ps_pred(goal_mask,j)').^2);
    end
    criterion=err_rec_target;
    crr(n)=min(criterion);
    %%
    
    %%
    pcoded_reconstructed=(ps_pred(:,find(criterion==min(criterion),1)))';
    
    %% compute desired joint angles
    p_reconstructed=net(pcoded_reconstructed(1:size(M_coded,2))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
    p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
    
    %% compute command (multiple possibilities)
    % %saturation (go in direction but cannot go furhter than dpmax
    dp=min(abs(p_reconstructed-p)+(rand(1,6)-1/2)*2*dpnoise,dpmax).*sign(p_reconstructed-p);
    % %speed command (goes in the direction with a factor 0.3
    %dp=(p_reconstructed-p)*0.3;
    % %speed with inertia takes into account previous speed
    %dp=(p_reconstructed-p)*0.5+dp_old*0.2;
    %%
    if 1
        figure(1)
        subplot(121)
        hold off
        plot_touch(p,x,body,500,'y','k','linewidth',4);
        hold on
        plot_agent(p,s_target0,body,'m','k','linewidth',4);
        axis([-2.5,2.5,-.01,2])
        axis equal
        subplot(122)
        hold off
        for i=61:size(MS,2)
            plot(zMi{i}(1,:),zMi{i}(2,:),'k.')
            hold on
        end
        toplot1=intersect(zMi{find(s_target0==max(s_target0))}',zMi{1}','rows')';
        toplotend=intersect(zMi{find(s_target0==max(s_target0))}',zMi{end}','rows')';
        plot(zMi{find(s_target0==max(s_target0))}(1,:),zMi{find(s_target0==max(s_target0))}(2,:),'b.')
        plot(toplot1(1,:),toplot1(2,:),'c.')
        plot(toplotend(1,:),toplotend(2,:),'m.')

%%
        hold on
        contour(extractdata(z1grid),extractdata(z2grid),reshape(err_grid,ngrid,ngrid))
        %contour(extractdata(z1grid),extractdata(z2grid),reshape(diffp_hist,ngrid,ngrid)) %for proprio error

        
        hold on
          plot(zmean_hist(1:n,1),zmean_hist(1:n,2),'r-+','markersize',10)
          plot((z_rec(1,criterion==min(criterion))),(z_rec(2,criterion==min(criterion))),'g.','markersize',20);
          axis equal
          %caxis([convergence_criterion .1])
          colorbar
%           c=colorbar;
%           set(c,'Limits',[convergence_criterion .1])
          axis([-2,4,-2,3])
        %             plot(ps_target_current(goal_mask))
        %             hold on
        %             plot(pcoded_reconstructed(goal_mask))
%         plot(crr,'.k-','markersize',20)
%         hold on
%         %plot(squeeze(ds_hist(rep,1:n)),'.r-','markersize',20)
%         drawnow
        %
        
        
        %hold on
        %plot(.5*exp(extractdata(zLogvar)))
        
    end
    %%
    if min(criterion)<convergence_criterion
        fprintf('finished @ n=%d, final dist sensory %2.2f\n',n,ds_hist(rep,n))
        break
    elseif n==windowstime
        fprintf('not converged crit=%2.4f, min dist sensory %2.2f\n',min(criterion),ds_hist(rep,n))
    end
    %
    %p=constmot(p+dp+dp_old*dp_inertia,m_const_min,m_const_max); %next pose
    p=max(min(p+dp+dp_old*dp_inertia,body.motor.motor_constraints_max),body.motor.motor_constraints_min);
    dp_old=dp;
end
    