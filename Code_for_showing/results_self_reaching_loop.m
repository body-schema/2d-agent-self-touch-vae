%% parameters
sensory_sensibility=.1;
criterion_base=5e-3;
goal_mask=[ones(1,size(M_coded,2)),ones(1,size(S,2))]>0;

%% control param
dpmax=0.1;dpnoise=0.;
dp_inertia=0; %inertia from last dp
windowstime=100;nz=50;sampling_spread=5;
%% display?
displ=1
%% start position
p0=[0.6223    1.7068    1.1631    0.2978    0.4697    1.7735]; %very
p0=[1.7735 0.4697  0.2978 1.1631 1.7068 0.6223]; %very
%
p0=[-0.7854,0.1,0.1,0.1,0.1,-0.7854];
%p0=[0.9464    1.3000    1.8    0.5142    0.3972    0.1136]
%p0=body.motor.motor_constraints_min;

p_hist=p0;

%% target positions
Nrep=4;
Lxvec=40;
xvec=linspace(2.1,sum(body.arm_lengths)-0.1,Lxvec); %easy 3.8

convergence_criterion=[];
n_convergence=[];
p_convergence=[];
crit_convergence=[];
crit_hist=NaN*zeros(length(xvec),Nrep,windowstime);
ds_hist_i=NaN*zeros(length(xvec),Nrep,windowstime);
ds_end=[];
for i=1:length(xvec)
    x=xvec(i);
    fprintf('external touch @ %2.2f\n',x)
    % compute target sensations
    s_target0=mannella_artificial_touch_continuous_position(x,body);
    goal_mask=[ones(1,size(M_coded,2)),s_target0]>0.1;
    %x2=5;
    %s_target0=max(s_target0,mannella_artificial_touch_continuous_position(x2,body));

    %goal_mask=[ones(1,size(M_coded,2)),s_target0]>0.1;
    %%
    convergence_criterion(i)=criterion_base;
    ds_hist=NaN*zeros(Nrep,windowstime);
    crst=NaN*zeros(Nrep,windowstime);
    for rep=1:Nrep
        
        
        %% init
        pcoded_history=[];
        ptarget_history=[];
        err_rec_history=[];
        errmin_rec_history=[];
        err_goal_history=[];
        err_rec_current_history=[];
        err_rec_current_history_goal=[];
        err_rec_actual_notsimul=[];
        dp_old=zeros(1,6);
        %%
        clf
        p=p0;
        crr=[];
        for n=1:windowstime
            %%
            p_coded = compute_activation(p,body);
            %%
            [Px,Py]=get_agent_limbs(p,body);
            [sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
            ps_real=[p_coded,sensory_real];
            %% FROM TARGET SENSATION COMPUTE TARGET MS
            ds_hist(rep,n)= mse(s_target0(s_target0>sensory_sensibility),sensory_real(s_target0>sensory_sensibility));
            
            s_target_real=max(s_target0,sensory_real);
            
            ps_target_current=[p_coded,s_target_real];
            
            ps_target_current_dl=dlarray(ps_target_current','CB');

            err_rec_target=[];
            err_conf=[];
            ps_pred=[];
            if displ==2
                figure(2)    
                hold off
            end
            for j=1:nz
                %% sample
                [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
                zm=z;
                ps_pred(:,j) = sigmoid(forward(decoderNet, zm));
                
                %% compute reconstruction error:
                err_rec_target(j)=mean((ps_target_current(goal_mask)-ps_pred(goal_mask,j)').^2);
                
                if displ==2
                                 
                ez=extractdata(z);
                plot(extractdata(zMean(1)),extractdata(zMean(2)),'r+','markersize',10)
                hold on
                plot(ez(1),ez(2),'k.','markersize',round(-log(err_rec_target(j))*20))
                axis equal
                axis([-3,3,-1,5])
                
                end
            end
            
            %%
            criterion=err_rec_target;
            crr(n)=min(criterion);
            crst(rep,n)=crr(n);
            %%
            
            %%
            pcoded_reconstructed=(ps_pred(:,find(criterion==min(criterion),1)))';
            
            %% compute desired joint angles
            p_reconstructed=net(pcoded_reconstructed(1:size(M_coded,2))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
            p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
            
            %% compute command (multiple possibilities)
            % %saturation (go in direction but cannot go furhter than dpmax
            dp=min(abs(p_reconstructed-p)+(rand(1,6)-1/2)*2*dpnoise,dpmax).*sign(p_reconstructed-p);
            % %speed command (goes in the direction with a factor 0.3
            %dp=(p_reconstructed-p)*0.3;
            % %speed with inertia takes into account previous speed
            %dp=(p_reconstructed-p)*0.3+dp_old*0.1;
            %%
            if displ
                figure(1)
                subplot(121)
                hold off
                plot_touch(p,x,body,500,'y','k','linewidth',4);
                hold on
                %plot_touch(p,x2,body,500,'y','k','linewidth',4);

                plot_agent(p,s_target0,body,'m','k','linewidth',4);
                axis([-2.5,2.5,-.01,2])
                axis equal
                subplot(122)
                hold off
                %             plot(ps_target_current(goal_mask))
                %             hold on
                %             plot(pcoded_reconstructed(goal_mask))
                plot(crr,'.k-','markersize',20)
                hold on
                %plot(squeeze(ds_hist(rep,1:n)),'.r-','markersize',20)
                drawnow
                %
              
                
                hold on
                plot(.5*exp(extractdata(zLogvar)))
                
            end
            %%
            if min(criterion)<convergence_criterion(i)
                fprintf('finished @ n=%d, final dist sensory %2.2f\n',n,ds_hist(rep,n))
                break
            elseif n==windowstime
                fprintf('not converged crit=%2.4f, min dist sensory %2.2f\n',min(criterion),ds_hist(rep,n))
            end
            %
            %p=constmot(p+dp+dp_old*dp_inertia,m_const_min,m_const_max); %next pose
            p=max(min(p+dp+dp_old*dp_inertia,body.motor.motor_constraints_max),body.motor.motor_constraints_min);
            dp_old=dp;
        end
        n_convergence(i,rep)=n;
        p_convergence(i,:,rep)=p;
        crit_convergence(i,rep)=min(criterion);
        
    end
    crit_hist(i,:,:)=crst;
    ds_hist_i(i,:,:)=ds_hist;
    ds_end(i,:)=nanmin(ds_hist');
end
%% compare criterion and sensory distances
rep=1;
figure(6)
 clf
for rep=1:Nrep
    crit_1=(squeeze(crit_hist(:,rep,:)));
    ds_1=(squeeze(ds_hist_i(:,rep,:)));
   
    plot(ds_1(:),crit_1(:),'.')
    hold on
end
line([0,max(ds_1(:))],[criterion_base,criterion_base],'color','r')
%% plot final postures

for i=1:Lxvec
    clf
x=xvec(i);

s_target0=mannella_artificial_touch_continuous_position(x,body);
subplot(211)
hold on
plot_agent(p0,s_target0,body,'w','k','linewidth',4);
rep=1;
for rep=1:Nrep
    p= squeeze(p_convergence(i,:,rep));

    %%
    plot_touch(p,x,body,500,'y','k','linewidth',4);
end 
axis equal
title(sprintf('%d',i))
subplot(212)
plot(squeeze(ds_hist_i(i,:,:))','.-')
hold on
line([n_convergence(i,:)',n_convergence(i,:)']',.5*[zeros(Nrep,1),ones(Nrep,1)]')
pause(1)
end