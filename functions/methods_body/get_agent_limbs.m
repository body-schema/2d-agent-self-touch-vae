function [Px,Py]=get_agent_limbs(M,body)

L=size(M,1);
Py=cumsum(sin(cumsum(M,2)).*repmat(body.arm_lengths(2:end),L,1),2);
Px=cumsum(cos(cumsum(M,2)).*repmat(body.arm_lengths(2:end),L,1),2);
Px=[-body.arm_lengths(1),0,Px];Py=[0,0,Py]; %add origins