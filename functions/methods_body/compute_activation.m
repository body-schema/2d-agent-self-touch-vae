function output=compute_activation(inputs,body)

for i=1:body.nb_motors
    output(:,(i-1)*body.propriosensors.nb+1:body.propriosensors.nb*i) = exp(-(inputs(:,i) - body.propriosensors.means(i,:)).^2./(2*body.propriosensors.radius(i,:).^2));
end