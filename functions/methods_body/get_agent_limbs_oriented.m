function [Px,Py]=get_agent_limbs_oriented(M,body)

[Px,Py]=get_agent_limbs(M,body);

torsoxy=[Px(4:5)',Py(4:5)'];
torso_taille = norm(diff(torsoxy));
torso_t=-mean(torsoxy);
torsoxy2=torsoxy+torso_t;
torso_ang=pi-atan2(torsoxy2(1,2),torsoxy2(1,1));
torso_R=[[cos(torso_ang) -sin(torso_ang)]; [sin(torso_ang) cos(torso_ang)] ];

P_new=torso_R*([Px;Py]+torso_t');
Px=P_new(1,:);
Py=P_new(2,:);