function s=mannella_artificial_touch_continuous_position(x,body)
%% generate an artifical touch on limb n_limb at distance x_percent of the limb length
s= exp( -(body.tactilesensors.means-x).^2./(2*body.tactilesensors.radius.^2) );

