function [s,X,intersection_indices,intersection_points2] = mannella_tactile_continuous(Px,Py,body,varargin)
% arm lengths vector
arm_lengths_cumsum = [0,cumsum(body.arm_lengths)];
%% mannella intersection
touch_onbody=[];X=[];
s = zeros(1,sum(body.tactilesensors.nb));
%%
%%find limb
[intersection_points2,intersection_indices]=InterX([Px;Py]);
if ~isempty(intersection_points2)
    for i=1:size(intersection_indices,1)
        % body section touched
        i1=intersection_indices(i,1);
        i2=intersection_indices(i,2);
        touch_onbody(2*(i-1)+1,1)=i1;
        touch_onbody(2*i,1)=i2;
        % position on limb in distance from limb start
        touch_onbody(2*(i-1)+1,2)=sqrt((intersection_points2(1,i)-Px(i1)).^2+(intersection_points2(2,i)-Py(i1)).^2);
        touch_onbody(2*i,2) = sqrt((intersection_points2(1,i)-Px(i2)).^2+(intersection_points2(2,i)-Py(i2)).^2);
        
        pos_touch1=touch_onbody(2*(i-1)+1,2)+arm_lengths_cumsum(i1);
        pos_touch2=touch_onbody(2*(i),2)+arm_lengths_cumsum(i2);
        
        X=[X;pos_touch1,pos_touch2];
        
        s1=exp(-(body.tactilesensors.means-pos_touch1).^2./(2*body.tactilesensors.radius.^2));
        s2=exp(-(body.tactilesensors.means-pos_touch2).^2./(2*body.tactilesensors.radius.^2));
        
        s=max(max(s,s1),s2);
        
    end
end
%
if nargin==4
    noise_var=varargin{1};
    s=min(max(0,s+rand(1,size(s,2))*sqrt(12*noise_var)),1);
end
%