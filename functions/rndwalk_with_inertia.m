function X=rndwalk_with_inertia(N,D,sigma,m,M0)

Rrnd=2*sqrt(sigma)*(rand(N,D)-0.5);

if nargin<5
R=[zeros(1,D);Rrnd];
M0=[zeros(1,D)];
else
R=[M0;Rrnd(1:end-1,:)];
end



if m~=0 %with inertia
    X=zeros(N,D);
    X(1,:)=R(1,:);
    X(2,:)=X(1,:)+R(2,:);
    for i=3:N
        %X(i,:)=X(i-1,:)+(m*(X(i-1,:)-X(i-2,:))-1/m*R(i,:))/(m+1/m);
        X(i,:)=X(i-1,:)+(m*(X(i-1,:)-X(i-2,:))+(1-m)*R(i,:));
    end
else %without inertia
    X=cumsum(R,1);
end
