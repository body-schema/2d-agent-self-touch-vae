function s=mannella_artificial_touch_continuous(n_limb,x_percent,sensors,arm_lengths)
%% generate an artifical touch on limb n_limb at distance x_percent of the limb length

arm_lengths_cumsum=[0,cumsum(arm_lengths)];
%%
spos=[];
for i=1:length(arm_lengths)
    sensors_pos_loc=(sensors.local_pos{i})*arm_lengths(i);
    if i>1
    spos=[spos,sum(arm_lengths(1:i-1))+sensors_pos_loc];
    else
        spos=sensors_pos_loc;
    end
end
sensors_radius=[sensors.radius{:}];
%%

tpos=x_percent*arm_lengths(n_limb)+arm_lengths_cumsum(n_limb);

s=zeros(1,sum(sensors.nb));

s= exp( -(spos-tpos).^2./(2*sensors_radius.^2) );



