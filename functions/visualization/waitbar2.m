function waitbar(i,N,pourcombien)
pour=floor(N/pourcombien);
lstr=length(int2str(N-pour));
lpoints=floor(pour/max(floor(pour/10),1))-max(ceil(1-(pour-1)/10),0);
if i==1
    fprintf('\n\n\nComputing forloop on %d :\n',N)
    fprintf('Iteration %s/%d : [%s]',repmat('0',1,lstr),N,repmat('?',1,lpoints));
elseif ~mod(i-1,pour)
    strC=repmat('\b',1,lstr+lpoints+length(int2str(N))+6);
    str0=[repmat('0',1,lstr-length(int2str(i-1))),int2str(i-1)];
    strO=sprintf('%s/%d : [%s]',str0,N,repmat('o',1,lpoints));
    fprintf([strC strO])
elseif i==N
    fprintf('\nDone with success ! Brava tortilla\n')
elseif ~mod(i,max(floor(pour/10),1))
    cnt=round(rem(i-1,pour)/max(floor(pour/10),1));
    strC=repmat('\b',1,lpoints+2-cnt);
    stro=repmat('0',1,lpoints-cnt);
    fprintf([strC '$' stro ']'])
end
