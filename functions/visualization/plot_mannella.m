function [Px,Py]=plot_mannella(Px,Py,body,s,varargin)
% afficher dans le bon sens attention a changer si changement de taille
torsoxy=[Px(4:5)',Py(4:5)'];
torso_t=-mean(torsoxy);
torsoxy2=torsoxy+torso_t;
torso_ang=pi-atan2(torsoxy2(1,2),torsoxy2(1,1));
torso_R=[[cos(torso_ang) -sin(torso_ang)]; [sin(torso_ang) cos(torso_ang)] ];

P_new=torso_R*([Px;Py]+torso_t');
Px=P_new(1,:);
Py=P_new(2,:);

plot_circle_body(Px,Py,body.tactilesensors.means,s,varargin)
