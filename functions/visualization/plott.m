function [varargout]=plott(X,varargin);
%adapt plot to variable
[A,B]=size(X);
if A<B
    N=B;
    dim=A;
    X=X';
else
    N=A;
    dim=B;
end


if nargin==1
    switch dim
        case 1
            h=plot(1:length(X),X);
             xlabel('n');ylabel('f(n)');
        case 2
            h=plot(X(:,1),X(:,2));
            xlabel('x');ylabel('y');
        case 3
            h=plot3(X(:,1),X(:,2),X(:,3));
    end
elseif isa(varargin{1},'double')
    V=varargin{1};
    if max(size(V))>1
        L=V;
    else
        L=1:V;
    end
    switch dim
        case 1
            h=plot(L,X(L),varargin{2:end});
            xlabel('n');ylabel('f(n)');
        case 2
            h=plot(X(L,1),X(L,2),varargin{2:end});
            xlabel('x');ylabel('y');
        case 3
            h=plot3(X(L,1),X(L,2),X(L,3),varargin{2:end});
            xlabel('x');ylabel('y');zlabel('z');
    end
else
    switch dim
        case 1
            h=plot(1:length(X),X,varargin{:});
             xlabel('n');ylabel('f(n)');
        case 2
            h=plot(X(:,1),X(:,2),varargin{:});
            xlabel('x');ylabel('y');
        case 3
            h=plot3(X(:,1),X(:,2),X(:,3),varargin{:});
            xlabel('x');ylabel('y');zlabel('z');
    end
end
grid on
if nargout
    varargout{1}=h;
end
