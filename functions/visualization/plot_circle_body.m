function plot_circle_body(Px,Py,xtouches,siz,color_touch,varargin)

arm_lengths=sqrt(diff(Px).^2+diff(Py).^2);
cumsum_length=[0,cumsum(arm_lengths)];

xtouch_local=[];
for i=1:length(xtouches)
    nlimb(i)=find(cumsum_length>xtouches(i),1)-1;
    xper(i)=(xtouches(i)-cumsum_length(nlimb(i)))./arm_lengths(nlimb(i));
    xtouch_local(i,:)=[Px(nlimb(i)),Py(nlimb(i))]+xper(i)*[Px(nlimb(i)+1)-Px(nlimb(i)),Py(nlimb(i)+1)-Py(nlimb(i))];
end
% afficher dans le bon sens attention a changer si changement de taille
torsoxy=[Px(4:5)',Py(4:5)'];

torso_t=-mean(torsoxy);
torsoxy2=torsoxy+torso_t;
torso_ang=pi-atan2(torsoxy2(1,2),torsoxy2(1,1));
torso_R=[[cos(torso_ang) -sin(torso_ang)]; [sin(torso_ang) cos(torso_ang)] ];

P_new=torso_R*([Px;Py]+torso_t');
Px=P_new(1,:);
Py=P_new(2,:);
%%
xxx_new=torso_R*(xtouch_local'+torso_t');
%% plot
%plott([Px;Py],varargin{:}); hold on
plott([Px;Py],varargin{:}); hold on
scatter(xxx_new(1,:),xxx_new(2,:),siz,'filled',color_touch,'markeredgecolor','k');
hold off