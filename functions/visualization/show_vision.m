function h=show_vision(I,body)
h=imagesc(reshape(I,body.camera.nb_pixel_x,body.camera.nb_pixel_y)');
axis xy %because images are shown with bad axis
