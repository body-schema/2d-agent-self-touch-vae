function [Px,Py]=plot_agent(p,s,body,varargin)
% show agent with tactile sensors activations
[Px,Py]=get_agent_limbs(p,body);

plot_circle_body(Px,Py,body.tactilesensors.means,round(s*100)+10,varargin{:});


