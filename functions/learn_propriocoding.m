%% learn coding to proprio
%% decoding proprioception
nt=min(N,5e4);
dims=[120,60,40,20];
net = feedforwardnet(dims,'trainscg');
net.layers{length(dims)+1}.transferFcn = 'tansig';
net.trainParam.epochs=1e4;

%normalize proprioception into [-1,1] activations
Mobj=(M-(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2)./((body.motor.motor_constraints_max-body.motor.motor_constraints_min))*2;
randid=randperm(size(M,1));
trainid=randid(1:nt);
% learn decoding model
% use parallel processing if available
net = train(net,M_coded(trainid,1:body.nb_motors*body.propriosensors.nb)',Mobj(trainid,1:body.nb_motors)','useParallel','yes');
%% test decoding model on new proprio
% y = net(M_coded(randid(nt+1:end),:)')';
% p_rec=y.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
%  p_rec=max(min(p_rec,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
% %perf = perform(net,M(nt+1:end,:)',y*pi/3+pi/3);
% perf = perform(net,M(randid(nt+1:end),:)',p_rec')
    %% show
if 0
    %%
    p=constmot(rand(100,8)*2*pi,body.motor.motor_constraints_min,body.motor.motor_constraints_max);
    p_coded=compute_activation(p,body)
    y = net(p_coded');
    perf = perform(net,p',y.*((body.motor.motor_constraints_max'-body.motor.motor_constraints_min'))/2+(body.motor.motor_constraints_max'+body.motor.motor_constraints_min')/2);
    %
    clf
    for k=1:6
        clf
        plot(y(k,:)'.*((body.motor.motor_constraints_max(k)-body.motor.motor_constraints_min(k)))/2+(body.motor.motor_constraints_max(k)+body.motor.motor_constraints_min(k))/2,'r')
        hold
        
        plot(p(:,k),'k')
        pause(1)
    end
end