function x=constmot(x,amin,amax)
%% return the triangle function with lower value am and upper value aM and identity between both values
% % % modmot=@(a)rem(a+sign(a)*pi,2*pi)-sign(a)*pi;

for i=1:size(x,2)
    aM=amax(i);
    am=amin(i);
% % %     if (aM==pi)&&(am==-pi)
% % %         x(:,i)=modmot(x(:,i));
% % %     else
        x(:,i)=(aM+am)/2+4*(aM-am)/(4*(aM-am)).*(abs(mod((x(:,i)-(aM+am)/2-(aM-am)/2),2*(aM-am))-(aM-am))-(aM-am)/2);
% % %     end
end
