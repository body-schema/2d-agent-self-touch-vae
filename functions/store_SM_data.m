%% to csv
%% store into single precision with floating point precision
simple_tactile_2d_agent=table();

simple_tactile_2d_agent.joint_angles=single(M.*(abs(M)>eps('single')));
simple_tactile_2d_agent.proprioception_activations=single(M_coded.*(abs(M_coded)>eps('single')));
simple_tactile_2d_agent.tactile_activations=single(S.*(abs(S)>eps('single')));
%%
writetable(simple_tactile_2d_agent,'table_test.csv')
%simple_tactile_2d_agent.arm_lenghts=body.arm_lengths;
%simple_tactile_2d_agent.motor_constraints=[body.motor.motor_constraints_min;body.motor.motor_constraints_max];

