function joint_angles=proprio_decoding(proprio_coded,net,body)
y = net(proprio_coded');
propriosize=1:net.Layers{end}.size;
joint_angles = (y.*((body.motor.motor_constraints_max(propriosize)'-body.motor.motor_constraints_min(propriosize)'))/2+(body.motor.motor_constraints_max(propriosize)'+body.motor.motor_constraints_min(propriosize)')/2)';