%% requirements:
%% - awgn() function within communication toolbox for corrupting data
%% - Functions folder in the same folder

addpath(genpath(pwd))
%%
clear all
close all
clc
%% body parameters
% body parameters
body.arm_lengths = [0.5,.8,.7,1.4,0.7,0.8,0.5]; %limbs lengths from left hand to rigth hand
body.nb_limbs = length(body.arm_lengths); 
body.L = sum(body.arm_lengths);
% motor parameters
body.nb_motors = body.nb_limbs-1;
body.motor.motor_constraints_min = [-pi/4,0,0,0,0,-pi/4]; %minimum joint angles constraints from left wrist to right wrist
body.motor.motor_constraints_max = [pi/2,3*pi/4,2*pi/3,2*pi/3,3*pi/4,pi/2]; %maximumjoint angles constraints from left wrist to right wrist
% tactile sensors parameters
body.tactilesensors.nb = 100; %number of sensors
body.tactilesensors.radiusmin = .2; % min radius tactile receptive fields
body.tactilesensors.radiusmax = .4; % max radius ractile receptive field
%% initialize tactile sensors
body.tactilesensors.means = sort(rand(1,body.tactilesensors.nb)*body.L); %uniformly sampled on the skin
body.tactilesensors.radius = rand(1,body.tactilesensors.nb).*(body.tactilesensors.radiusmax-body.tactilesensors.radiusmin)+body.tactilesensors.radiusmin;
%% initialize proprioceptive sensors
body.propriosensors.nb = 20; %nb of proprioceptive senosrs per joint angle
body.propriosensors.radiusmin = 0.4*(body.motor.motor_constraints_max-body.motor.motor_constraints_min); % min size of proprio receptive fields, rescaled by motor constraints
body.propriosensors.radiusmax = 0.6*(body.motor.motor_constraints_max-body.motor.motor_constraints_min); % max size of proprio receptive fields, rescaled by motor constraints
%% initialize proprio sensors
for i = 1 : body.nb_motors
    body.propriosensors.means(i,1:body.propriosensors.nb) = sort(rand(1,body.propriosensors.nb)* (body.motor.motor_constraints_max(i)-body.motor.motor_constraints_min(i)) + body.motor.motor_constraints_min(i)); %uniformly sampled on the skin
    body.propriosensors.radius(i,1:body.propriosensors.nb) = rand(1,body.propriosensors.nb).*(body.propriosensors.radiusmax(i)-body.propriosensors.radiusmin(i))+body.propriosensors.radiusmin(i);
end
%% Simulation parameters
%movement parameters
N = 1e4; %sample points
sigma_rnd = .9; mrand = 0.9; %random walk parameters (sigma_rnd is step size), mrand is some inertia parameter <1 for a more biological motion. If mrand=0, traditional randomwolk with random direction and speed. The more mrand is close to 1 the more th
%% Generate motion and joint angles data
%generate motor actions using custom random walk.
disp('Generate random walk motion')
M = constmot(rndwalk_with_inertia(N,body.nb_motors,sigma_rnd,mrand,body.motor.motor_constraints_min),body.motor.motor_constraints_min,body.motor.motor_constraints_max);

%% From there its repeatble
if 0
    datestr=cellstr(datetime('now','Format', 'd-m-yy-HHmm'));
    save(['/home/abdelnasser/PostDoc/Datasets/VAE_Mannela_2/BodyMotor_',datestr{:},'.mat'],'body','M','sigma_rnd','mrand')
end
%% Load data here
if 0
    %%
    load('/home/abdelnasser/PostDoc/Datasets/VAE_Mannela_2/BodyMotor_28-16-22-1616.mat')
    N=size(M,1);
end
%% encode proprio using body parameters
M_coded = compute_activation(M,body);
%% initiliazes touch
noise_var=0; %additive noise inputs

S = zeros(N,body.tactilesensors.nb); %tactile inputs\

%% show movements
if 0
    show_movements
end
%% cmpute sensory output and touches during movements
disp('Compute sensory inputs')
X = []; %double touch positions limbs
L_touched = []; %touched limbs
for n = 1:N
    waitbar2(n,N,10) %a waiting bar
    % gnerated [proprioceptive inputs
    
    % current body posture
    [Px,Py]=get_agent_limbs(M(n,:),body);
    
    % generated tactile inputs
    [S(n,:),x_touched,L] = mannella_tactile_continuous(Px,Py,body); %compute tactile activations
    
    % generated touches positions
    if ~isempty(x_touched)
        X=[X;[repmat(n,size(x_touched,1),1),x_touched]]; %double touched positions
        L_touched=[L_touched;[repmat(n,size(L,1),1),L]];
    end
end
%% classical stats
if 0
    V=L_touched(:,2:3);
    U=unique(V,'rows');
    nU=[];
    for i=1:size(U,1)
        nU(i)=sum(all(V==U(i,:),2));
    end
    stats=[U,nU',round(nU'./sum(nU)*100)]
    % show the repartition of touches
    
    figure(1)
    clf
    hist(reshape(X(:,2:3),2*size(X,1),1),100)
    hold on
    c=jet(body.nb_limbs);
    cL=[0,cumsum(body.arm_lengths)];
    for i=1:length(cL)-1
        line([cL(i),cL(i+1)],[0,0],'linewidth',10,'color',c(i,:))
        %text((cL(i)+cL(i+1))/2,-1,sprintf('limb %d',i))
        hold on
    end
    legend({'Distribution of touches','Left hand','Left forearm','Left arm','Torso','Right arm','Right forearm','Right arm'})
    xlabel('position of touch on skin')
    ylabel('number of touches')
    set(gca,'fontsize',20)
    axis([0,5,0,1800])
    %% stats of multiple touches
    B = unique(L_touched(:,1)); % which will give you the unique elements of A in array B
    Ncount = histc(L_touched(:,1), B);
    disp('Stats on multiple touches')
    fprintf('Number of double touch: %d/%d\n',sum(Ncount==1),length(Ncount));
    fprintf('Number of more than double touch: %d/%d\n',sum(Ncount>1),length(Ncount));
    fprintf('Number of triple touch: %d/%d\n',sum(Ncount==2),length(Ncount));
    fprintf('Number of quadriple touch: %d/%d\n',sum(Ncount==3),length(Ncount));
    fprintf('Number of quintuple touch: %d/%d\n',sum(Ncount==4),length(Ncount));
    fprintf('Number of sextuple touch: %d/%d\n',sum(Ncount==5),length(Ncount));
end
%% list of saved paramters
%% show movements and  sensorimotor outpus
video=0; %put to 1 to record a video
if 0
    %%
    if video
        datestr=cellstr(datetime('now','Format', 'yy-MM-dd-HH-mm'));
            writerObj = VideoWriter(['~/PostDoc/Datasets/VAE_Mannela_2/random_exploration',datestr{:},'.avi']);
            writerObj.FrameRate = 5;
            open(writerObj);
    end
    figure(1)
    clf
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for n = 2   :500
        [Px,Py]=get_agent_limbs(M(n,:),body);
        
        subplot(121)
        %plot_mannella(Px,Py,sensors,S(n,:),1);
        plot_circle_body(Px,Py,body.tactilesensors.means,round(S(n,:)*100)+10,'m','k','linewidth',4)
        hold off
        title(sprintf('frame %d',n))
        axis equal
        axis([-2,2,-.5,2])
        axis off
        subplot(424)
        imagesc(S(n,:))
        colorbar
        caxis([0,1])
        title('Tactile activations')
        subplot(426)
        imagesc(M_coded(n,:))
        colorbar
        title('Proprioceptive activations')
        colorbar
        caxis([0,1])
        colormap('gray')
        drawnow
        if video
            frame=getframe(gcf);
            writeVideo(writerObj, frame);
        end
        %         drawnow
    end
elseif 0
     % create the video writer with 1 fps
    writerObj = VideoWriter('myVideo.avi');
    writerObj.FrameRate = 160;   
    % open the video writer
    open(writerObj);
    %%
    f=figure(1);
    clf
    set(f,'Position',[100,100,2000,1000]);
    colormap('gray');
    caxis([0 1]);
%     
    fsub1=subplot(121,'Parent',f);
    set(fsub1,'Position',[0.100 0.0300 0.4347 2.150]);
    axis off
    axis equal
    axis([-3 3 -1 2]);
    
    
    fsub2=subplot(222,'Parent',f);
    set(fsub2,'Position',[.57,.6,.33,.1]);
    colorbar('Ticks',[0,1]);
    colormap('gray');
    caxis([0 1]);
    title('tactile activations','Fontsize',20)
    
    fsub3=subplot(224,'Parent',f);
    set(fsub3,'Position',[.57,.4,.33,.1]);
    colormap('gray');
    caxis([0 1]);
    colorbar('Ticks',[0,1]);
    title('proprioceptive activations','Fontsize',20)
    for n=1:N
        waitbar2(n,N,10)
        [Px,Py]=get_agent_limbs(M(n,:),body);
        %%
        %fsub1=subplot(121,'Parent',f);
        subplot(fsub1)
        set(fsub1,'Position',[0.100 0.0000 0.4347 1.]);
        plot_circle_body(Px,Py,body.tactilesensors.means,round(S(n,:)*100)+10,'k','linewidth',4);
        hold off
        title(sprintf('frame %d',n),'Fontsize',14)
        axis off
        axis equal
        axis([-3 3 -1 2]);
       
        
        
        %fsub2=subplot(222,'Parent',f);
        subplot(fsub2)
        set(fsub2,'Position',[.57,.6,.33,.1]);
        imagesc(S(n,:));
        colorbar('Ticks',[0,1]);
        colormap('gray');
        caxis([0 1]);
            title('tactile activations','Fontsize',20)

        %fsub3=subplot(224,'Parent',f);
        subplot(fsub3)
        set(fsub3,'Position',[.57,.4,.33,.1]);
        imagesc(M_coded(n,:))   ;
        colormap('gray');
        caxis([0 1]);
        colorbar('Ticks',[0,1]);
        title('proprioceptive activations','Fontsize',20)
        
        %drawnow
        frame= getframe(gcf);
        writeVideo(writerObj, frame);
        
    end
end
    if video
        close(writerObj)
    end
%%
%% VAE LEARNING
% create corrupted and target datasets
disp('Create the training and testing dataset, corrupting training dataset for a denoising framework for the VAE')
denoising_dataset
%% Create and launch network training
close all
disp('Start training the VAE')
DVAE_Mannella
%% learn decoding proprioceptive model
% use parpool if available
disp('Learn decoding of the proprioception encoding')
learn_propriocoding
%% test 
disp('Show some result for reaching to self')
show_results_reaching