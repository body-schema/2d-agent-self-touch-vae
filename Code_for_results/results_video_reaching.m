%% images reaching
close all
%% control param
dpmax=0.1;dpnoise=0.0;
dp_inertia=0; %inertia from last dp
windowstime=200;nz=100;sampling_spread=1;

%% start postion
p0=[0.1,0.1,0.1,0.1,0.1,0.1];
p=p0;
p_hist=p;
%% target position
x=4.5; %easy 3.8
s_target0=mannella_artificial_touch_continuous_position(x,body);
s_target=s_target0;

%% goal mask
goal_mask=[ones(1,size(M_coded,2)),ones(1,size(S,2))]>0;
weights=[ones(1,size(MS,2))];
stop_crit=5e-3;
% weights(1:size(M_coded,2))=[ones(1,size(M_coded,2)/2),ones(1,size(M_coded,2)/2)];
% weights(size(M_coded,2)+95)=100
weights=weights./sum(weights);
%% init
pcoded_history=[];
ptarget_history=[];
err_rec_history=[];
errmin_rec_history=[];
err_goal_history=[];
err_rec_current_history=[];
err_rec_current_history_goal=[];
err_rec_actual_notsimul=[];
criterion_history=[];
diffp_hist=[];
XTouch_hist={};
criterion2=0;
dp_old=zeros(1,6);
clear ps_pred
%%
datestr=cellstr(datetime('now','Format', 'yy-MM-dd-HH-mm'));

 writerObj = VideoWriter(['/home/abdelnasser/PostDoc/Datasets/VAE_Mannela_2/results_reaching_',datestr{:},'.avi']);
 writerObj.FrameRate = 5;
  open(writerObj);
%%
figure(1)
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
%% init
for i=1:10
        clf
    p0_coded = compute_activation(p,body);
    
    
    subplot(221)

    if i<=5
        ps_target_current=[p0_coded,zeros(1,size(S,2))];
        ps_target_current_dl=dlarray(ps_target_current','CB');
        [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
        zm=zMean;
        ps_pred= sigmoid(forward(decoderNet, zm));
        pcoded_reconstructed = dlarray(ps_pred,'BC');
        err_rec_target=extractdata((ps_target_current(goal_mask)-pcoded_reconstructed(goal_mask)).^2)*weights';
        %
        plot_agent(p0,zeros(1,size(S,2)),body,'m','k','linewidth',6);
    else
        ps_target_current=[p0_coded,s_target0];
        ps_target_current_dl=dlarray(ps_target_current','CB');
        [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
        zm=zMean;
        ps_pred= sigmoid(forward(decoderNet, zm));
        pcoded_reconstructed = dlarray(ps_pred,'BC');
        err_rec_target=extractdata((ps_target_current(goal_mask)-pcoded_reconstructed(goal_mask)).^2)*weights';
        %
        plot_agent(p0,s_target0,body,'m','k','linewidth',6);
        plot_touch(p0,x,body,500,'y','k','linewidth',4);
    end
    axis equal
    axis([-2.5,2.5,-.01,2])
    axis off
    title('Current vs generated configurations','fontsize',16)
    
    subplot(6,2,7)
    imagesc(ps_target_current(1:size(M_coded,2)))
    caxis([0,1])
    title('Current proprioception','fontsize',16)
    subplot(6,2,9)
    imagesc(extractdata(pcoded_reconstructed(1:size(M_coded,2))))
    caxis([0,1])
    title('Best generated proprioception','fontsize',16)
    subplot(6,2,11)
    imagesc(abs(ps_target_current(1:size(M_coded,2))-extractdata(pcoded_reconstructed(1:size(M_coded,2)))))
    caxis([0,1])
    title('Proprioceptive prediction error','fontsize',16)
    
      subplot(6,2,8)
    imagesc(ps_target_current(size(M_coded,2)+1:end))
    caxis([0,1])
     title('Current tactile sensation','fontsize',16)
    subplot(6,2,10)
    imagesc(extractdata(pcoded_reconstructed(size(M_coded,2)+1:end)))
    caxis([0,1])
    caxis
    title('Best generated tactile sensation','fontsize',16)
    subplot(6,2,12)
    imagesc(abs(ps_target_current(size(M_coded,2)+1:end)-extractdata(pcoded_reconstructed(size(M_coded,2)+1:end))))
    caxis([0,1])
    colormap('gray')
    title('Tactile prediction error','fontsize',16)
    %%
    subplot(222)
    hold off
    semilogy(0,err_rec_target,'k.','markersize',5)
    hold on
    semilogy(0,err_rec_target,'r.-','markersize',20)
    title('Reconstruction criterion','fontsize',16)
    %%
    drawnow
        frame=getframe(gcf);
    writeVideo(writerObj, frame);
end

%%
for n=1:windowstime
    n
    %%
    p_coded = compute_activation(p,body);
    %%
    [Px,Py]=get_agent_limbs(p,body);
    [sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
    ps_real=[p_coded,sensory_real];
    %% FROM TARGET SENSATION COMPUTE TARGET MS
    s_target_real=max(s_target,sensory_real);
    
    ps_target_current=[p_coded,s_target_real];
    
        
    ps_target_current_dl=dlarray(ps_target_current','CB');
    
    %criterion2(n)=extractdata((ps_target_current_dl-pcoded_reconstructed').^2)'*weights';%extractdata(mean(((ps_target_current_dl-pcoded_reconstructed').^2)*weights'));

    err_rec_target=[];
    err_conf=[];
    subplot(221)
    hold off
    tic
    for j=1:nz
        if 0%j==1&&n>1
            ps_pred(:,j)=pcoded_reconstructed;
        else
            [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
            zm=z;
            ps_pred(:,j) = sigmoid(forward(decoderNet, zm));
        end
        pcoded_reconstructed = dlarray((ps_pred(:,j)),'BC');

        %% compute reconstruction error:
        
        %err_rec_target(j)=mean((ps_target_current(goal_mask)-pcoded_reconstructed(goal_mask)).^2);
        err_rec_target(j)=extractdata((ps_target_current(goal_mask)-pcoded_reconstructed(goal_mask)).^2)*weights';
        
        %%
        p_reconstructed=net((extractdata(pcoded_reconstructed(1:size(M_coded,2)))'))'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
        p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
        
        s_reconstructed=extractdata(pcoded_reconstructed(size(M_coded,2)+1:end));
        %%
        if j<10
            plot_agent(p_reconstructed,s_reconstructed,body,'w','color',[.5,.5,.5],'linewidth',1);
            hold on
        end
    end
    
    %%
    criterion=err_rec_target;
    %%
    pcoded_reconstructed=dlarray((ps_pred(:,find(criterion==min(criterion),1))),'BC');
    
    %%
    p_reconstructed=net((extractdata(pcoded_reconstructed(1:size(M_coded,2))))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
    p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
    s_reconstructed=extractdata(pcoded_reconstructed(size(M_coded,2)+1:end));



    plot_agent(p_reconstructed,s_reconstructed,body,'r','r','linewidth',2);

    hold on
    %change objective touched
    
    pcoded_history(n,:)=pcoded_reconstructed;
    ptarget_history(n,:)=ps_target_current;
    errmin_rec_history(n)=min(err_rec_target);
    err_rec_history(n,:)=(err_rec_target);
    fprintf('Error goal simulated: %2.4f\n', min(err_rec_target));
    
    
    sensoryactivation_desired=pcoded_reconstructed(:,size(p_coded,2)+1:end);
    %     %compute desired proprio angles

    %% different kind of commands   
    % %saturation (go in direction but cannot go furhter than dpmax
    dp=min(abs(p_reconstructed-p)+(rand(1,6)-1/2)*2*dpnoise,dpmax).*sign(p_reconstructed-p);
    % %speed command (goes in the direction with a factor 0.3
    %dp=(p_reconstructed-p)*0.3;
    % %speed with inertia takes into account previous speed
    %dp=(p_reconstructed-p)*0.2+dp_old*0.05;
    %show
    
    subplot(221)
    %%
    plot_agent(p,s_target_real,body,'m','k','linewidth',6);
    plot_touch(p,x,body,500,'y','k','linewidth',4);
    hold on
    if ~isempty(Xtouch)
        plot_touch(p,Xtouch,body,400,'c','k','linewidth',4);
    end
   
   
    axis equal
    axis([-2.5,2.5,-.01,2])
    axis off
    title('Current vs generated configurations','fontsize',16)

%     subplot(212)
%     hold off
%     %stem(s_target_real,'filled','linewidth',2,'color','b')
%     plot(ps_real,'o','linewidth',2,'Color',[1,.1,0]*.5,'markersize',4)
%     hold on
%     plot(extractdata(pcoded_reconstructed),'s','color',[1,.6,.6],'linewidth',2,'markersize',4)
%     stem(abs(ps_real-extractdata(pcoded_reconstructed)),'_','color',[1,.9,.5],'linewidth',1)
%     c=copper(body.nb_motors);
%     c1=[.9,.9,1];c2=c1;
%     c=[c1;c1;c1;c2;c2;c2].*[1;.8;.6;.4;.2;0];
%     cL=[0,cumsum(body.propriosensors.nb*ones(1,body.nb_limbs-1))];
%     for i=1:length(cL)-1
%         line([cL(i),cL(i+1)],[-0.04,-0.04],'linewidth',10,'color',c(i,:))
%         %text((cL(i)+cL(i+1))/2,-1,sprintf('limb %d',i))
%         hold on
%     end
%     
%     c=flag(body.nb_limbs);
%     c1=[1,.4,.4 ];c2=c1;
%     
%     c=[c1;c1;c1;c1;c2;c2;c2].*[1;.8;.7;.5;.3;.15;0];
%     [r,~ ]=find(([cumsum(body.arm_lengths)]'>body.tactilesensors.means)&([0,cumsum(body.arm_lengths(1:end-1))]'<body.tactilesensors.means));
%     for i=1:body.nb_limbs
%         line(size(M_coded,2)+[find(r==i,1,'first')-1,find(r==i,1,'last')],[-0.04,-0.04],'linewidth',10,'color',c(i,:))
%         %text((cL(i)+cL(i+1))/2,-1,sprintf('limb %d',i))
%         hold on
%     end
%     grid off
%     legend({'Actual touch','Generated touch','pred. error'})
%%
%     subplot(714)
%         c1=[.9,.9,1];c2=c1;
%     c=[c1;c1;c1;c2;c2;c2].*[1;.8;.6;.4;.2;0];
%     cL=[0,cumsum(body.propriosensors.nb*ones(1,body.nb_limbs-1))];
%     for i=1:length(cL)-1
%         line([cL(i),cL(i+1)],[-0.04,-0.04],'linewidth',10,'color',c(i,:))
%         %text((cL(i)+cL(i+1))/2,-1,sprintf('limb %d',i))
%         hold on
%     end
%     
%     c=flag(body.nb_limbs);
%     c1=[1,.4,.4 ];c2=c1;
%     
%     c=[c1;c1;c1;c1;c2;c2;c2].*[1;.8;.7;.5;.3;.15;0];
%     [r,~ ]=find(([cumsum(body.arm_lengths)]'>body.tactilesensors.means)&([0,cumsum(body.arm_lengths(1:end-1))]'<body.tactilesensors.means));
%     for i=1:body.nb_limbs
%         line(size(M_coded,2)+[find(r==i,1,'first')-1,find(r==i,1,'last')],[-0.04,-0.04],'linewidth',10,'color',c(i,:))
%         %text((cL(i)+cL(i+1))/2,-1,sprintf('limb %d',i))
%         hold on
%     end
%     grid off
%     axis([0,size(MS,2),-.5,.5])
%     axis off
    %%
    subplot(6,2,7)
    imagesc(ps_target_current(1:size(M_coded,2)))
    caxis([0,1])
    title('Current proprioception','fontsize',16)
    subplot(6,2,9)
    imagesc(extractdata(pcoded_reconstructed(1:size(M_coded,2))))
    caxis([0,1])
    title('Best generated proprioception','fontsize',16)
    subplot(6,2,11)
    imagesc(abs(ps_target_current(1:size(M_coded,2))-extractdata(pcoded_reconstructed(1:size(M_coded,2)))))
    caxis([0,1])
    title('Proprioceptive prediction error','fontsize',16)
    
      subplot(6,2,8)
    imagesc(ps_target_current(size(M_coded,2)+1:end))
    caxis([0,1])
     title('Current tactile sensation','fontsize',16)
    subplot(6,2,10)
    imagesc(extractdata(pcoded_reconstructed(size(M_coded,2)+1:end)))
    caxis([0,1])
    caxis
    title('Best generated tactile sensation','fontsize',16)
    subplot(6,2,12)
    imagesc(abs(ps_target_current(size(M_coded,2)+1:end)-extractdata(pcoded_reconstructed(size(M_coded,2)+1:end))))
    caxis([0,1])
    colormap('gray')
    title('Tactile prediction error','fontsize',16)
    %%
    subplot(222)
    hold off
    
    
    %plot(criterion_history,'.-','markersize',20)
    %plot(err_rec_history,'.-','markersize',20)
    %plot(criterion_history,'.-','markersize',20)
    semilogy(err_rec_history(:,1:j),'k.','markersize',5)
    hold on
    semilogy(errmin_rec_history,'r.-','markersize',20)
    %semilogy(criterion2,'b.-')
   
    

    %plot(diffp_hist,'b.-','markersize',20)
    title('Reconstruction criterion','fontsize',16)
    
    
drawnow
%%
    frame=getframe(gcf);
    writeVideo(writerObj, frame);
    %%
    if min(criterion)<stop_crit
        'finished'
        break
    end
      %  
    %p=constmot(p+dp+dp_old*dp_inertia,m_const_min,m_const_max); %next pose
    p=max(min(p+dp+dp_old*dp_inertia,body.motor.motor_constraints_max),body.motor.motor_constraints_min);
    p_hist(end+1,:)=p;
    
    dp_old=dp;
    
end
close(writerObj)