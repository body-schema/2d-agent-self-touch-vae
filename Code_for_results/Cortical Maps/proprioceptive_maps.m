%% show maps
itouch=find(sum(S,2)>1);
MSt=[M_coded(itouch(1:end),:),S(itouch(1:end),:)];
MSt_dl=dlarray(single(MSt'),'CB');
[~, zmt, zLogvar] = sampling(encoderNet, MSt_dl);
zMt=extractdata(zmt);

%% create neurons sensitive to left arm positions during touch
index_leftproprio=1:body.propriosensors.nb*body.nb_motors/2;
index_rightproprio=body.propriosensors.nb*body.nb_motors/2+1:body.propriosensors.nb*body.nb_motors;

M_coded_left=MSt(:,index_leftproprio);
M_coded_right=MSt(:,index_rightproprio);

Nclust=5;
[idx_left,C_left] = kmeans(M_coded_left,Nclust);
[idx_right,C_right] = kmeans(M_coded_right,Nclust);
%%
figure(1)
clf
cm=jet(Nclust);

hold on

for i=1:Nclust
    subplot(221)
    ik=find(idx_left==i,10);
    for j=1:10
    [Px,Py]=get_agent_limbs_oriented(M(itouch(ik(j)),:),body);

    plott([Px;Py],'k','linewidth',.5);
    hold on
    plott([Px(1:4);Py(1:4)],'color',cm(i,:),'linewidth',4);
    %plott([Px(5:8);Py(5:8)],'color',[.5,.5,.5],'linewidth',1);
    end
    axis equal
    axis([-2,2,-.5,2])
    axis off
    
    subplot(222)
    hold on
     scatter3(zMt(1,idx_left==i),zMt(2,idx_left==i),zMt(3,idx_left==i),10,cm(i,:),'filled')
     view(90,0)
     xlabel('z_1')
    ylabel('z_2')
    zlabel('z_3')
     %%
      subplot(223)
    ik=find(idx_right==i,10);
    for j=1:10
    [Px,Py]=get_agent_limbs_oriented(M(itouch(ik(j)),:),body);

    plott([Px;Py],'k','linewidth',.5);
    hold on
    plott([Px(5:8);Py(5:8)],'color',cm(i,:),'linewidth',4);
    end
    axis equal
    axis([-2,2,-.5,2])
    axis off
    
    subplot(224)
    hold on
     scatter3(zMt(1,idx_right==i),zMt(2,idx_right==i),zMt(3,idx_right==i),10,cm(i,:),'filled')
    view(90,0)
    xlabel('z_1')
    ylabel('z_2')
    zlabel('z_3')
end


