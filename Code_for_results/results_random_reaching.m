%% results test bed
%% parameters
sensory_sensibility=.1
%% control param
dpmax=0.4;dpnoise=0; dp_inertia=0; %inertia from last dp
windowstime=20;
convergence_criterion=8e-3;
nz=20;sampling_spread=5;
%%
n_convergence=[];
p_convergence=[];
ds_convergence=[];
dx_convergence=[];
crit_convergence=[];
x_k=[];
%%
K=1000;
for k=1:K
    fprintf('>>>>>>>>>>>>>>> k = %d/%d\n', k,K)
    %% random init position not a touch
    istouch=1;
    clf
    hold on
    while istouch
        m = constmot(rand(1,body.nb_motors)*100,body.motor.motor_constraints_min,body.motor.motor_constraints_max);
        m_coded = compute_activation(m,body);
        [Px,Py]=get_agent_limbs(m,body);
        % generated tactile inputs
        [s,x_touched,L] = mannella_tactile_continuous(Px,Py,body); %compute tactile activations
        plot_agent(m,s,body,'m','k','linewidth',6);
        
        if isempty(x_touched)
            istouch=0;
        end
    end
    m_start(k,:)=m;
    %% random touch position
    x=rand*sum(body.arm_lengths);
    
    x_k(k)=x;
    %% run
    s_target0=mannella_artificial_touch_continuous_position(x,body);
    weights=[ones(1,size(MS,2))];
    %weights(size(M_coded,2)/2+1:size(M_coded,2))=0;
    %weights(size(M_coded,2)+1:end)=(s_target>0.1)*10+1;
    weights=weights./sum(weights);  
    %goal_mask=[ones(1,size(M_coded,2)),s_target0]>0.1;
    goal_mask=ones(1,size(MS,2))>0;
    ds_hist=[];
    dx_hist=[];
    for n=1:windowstime
        fprintf('n=%d\n ',n)
        p_coded = compute_activation(p,body);
        %%
        [Px,Py]=get_agent_limbs(p,body);
        [sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
        if ~isempty(Xtouch)
        dx_hist(n)=min(abs(x-Xtouch(:)));
        else
            dx_hist(n)=NaN;
        end
        ps_real=[p_coded,sensory_real];
        %% FROM TARGET SENSATION COMPUTE TARGET MS
        
        s_target_real=max(s_target0,sensory_real);
        ds_hist(n)= mse(s_target0(s_target0>sensory_sensibility),sensory_real(s_target0>sensory_sensibility));
        
        ps_target_current=[p_coded,s_target_real];
        
        ps_target_current_dl=dlarray(ps_target_current','CB');
        
        err_rec_target=[];
        err_conf=[];
        ps_pred=[];
        for j=1:nz
            %% sample
            [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),sampling_spread);
            zm=z;
            ps_p = dlarray(sigmoid(forward(decoderNet, zm)),'BC');
            
            ps_pred(:,j)=ps_p;
            
            %% compute reconstruction error:
            err_rec_target(j)=extractdata((ps_target_current(goal_mask)-ps_p(goal_mask)).^2)*weights';
            %err_rec_target(j)=mean((ps_target_current(goal_mask)-ps_p(goal_mask)').^2);
        end
        %%
        criterion=err_rec_target;
        %%
        pcoded_reconstructed=(ps_pred(:,find(criterion==min(criterion),1)))';
        %% compute desired joint angles
        p_reconstructed=net(pcoded_reconstructed(1:size(M_coded,2))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
        p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
        %min(criterion)
        if min(criterion)<convergence_criterion
            fprintf('finished @ n=%d, final dist touch %2.2f\n',n,dx_hist(n))
            break
        elseif n==windowstime
            fprintf('not converged crit=%2.4f, min dist touch %2.2f\n',min(criterion),dx_hist(n))
        end
            
        %% compute command (multiple possibilities)
        % %saturation (go in direction but cannot go furhter than dpmax
        dp=min(abs(p_reconstructed-p)+(rand(1,6)-1/2)*2*dpnoise,dpmax).*sign(p_reconstructed-p);
        p=max(min(p+dp+dp_old*dp_inertia,body.motor.motor_constraints_max),body.motor.motor_constraints_min);
        dp_old=dp;
    end
    ds_convergence(k)=ds_hist(n);
    dx_convergence(k)=dx_hist(n);
    n_convergence(k)=n;
    p_convergence(k,:)=p;
    crit_convergence(k)=min(criterion);
end
%% stop, is it touch, distance
clf
subplot(121)
plot(x_k,n_convergence,'.')
subplot(122)
plot(x_k,ds_convergence,'.')
hold on
plot(x_k,dx_convergence,'.')
%%
clf
[N,EDGES,BIN] = histcounts(x_k,30);
EE=EDGES(1:end-1)+diff(EDGES)/2;
subplot(121)
boxplot(crit_convergence,EE(BIN))
subplot(122)
boxplot(dx_convergence,EE(BIN))

%%
mm=[]
for i=1:50
    
    mm(i)=nanmean(dx_convergence(BIN==i))
    mm(i)=nanmean(dx_convergence(BIN==i))
end
clf
plot(EE,mm,'.-')
%%
close all
boxplot(dx_convergence,EE(BIN))
axis([0,31,0,3])
xlabel('External Contact position')
ylabel('Final contact distance on skin')
set(gca,'Fontsize',16)
