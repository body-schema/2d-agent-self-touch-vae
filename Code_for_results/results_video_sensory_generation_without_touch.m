%% video plot simulation sensory
inottouch=find(sum(S,2)==0);

nnt=20;
p0vec=M(inottouch(1:nnt),:);
p0vec=[zeros(nnt,1),ones(nnt,1)*1,ones(nnt,1).*linspace(1,2,nnt)',ones(nnt,3)*1.4]
smean=[];
smm=[];
figure(1)
for i=1:nnt
    
    p=p0vec(i,:);
    p0_coded = compute_activation(p,body);
    ps_target_current_dl=dlarray([p0_coded,zeros(1,size(S,2))]','CB');
    clf
    for j=1:100
    [z, zMean, zLogvar] = sampling_distance(encoderNet, gather(	ps_target_current_dl),10);
    zm=z;   
    
    ps_pred= sigmoid(forward(decoderNet, zm));
    pcoded_reconstructed = dlarray(ps_pred,'BC');
    s_reconstructed=extractdata(pcoded_reconstructed(size(M_coded,2)+1:end));
    
    smean(j,:)=s_reconstructed;
    
    
    end
    smm(i,:)=median(smean);
    subplot(211)
    hold off
    plot_agent(p,smm(i,:),body,'m','k','linewidth',6);
     axis equal
    axis([-2.5,2.5,-.01,2])
    axis off
    subplot(212)
    hold on
    plot(smm(i,:))
    
    %imagesc(s_reconstructed)
    pause(.1)
end