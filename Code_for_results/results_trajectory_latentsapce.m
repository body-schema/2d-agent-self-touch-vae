%% latent trajectory

itouch=find(sum(S,2)>1);
MSt=[M_coded(itouch(1:end),:),S(itouch(1:end),:)];
MSt_dl=dlarray(single(MSt'),'CB');
[~, zmt, zLogvar] = sampling(encoderNet, MSt_dl);
zMt=extractdata(zmt);

%Take random 1000:
I=randperm(size(zMt,2),5000);
zI=zMt(:,I);
%%
G=graph(DkNN(squareform(pdist(zI')),5));
G=graph(DkEps(squareform(pdist(zI')),.24));
%max(G.conncomp)
%%
datestr=cellstr(datetime('now','Format', 'yy-MM-dd-HH-mm'));
video=0;
if video
 writerObj = VideoWriter(['/home/abdelnasser/PostDoc/Datasets/VAE_Mannela_2/results_trajectory_latent_',datestr{:},'.avi']);
 writerObj.FrameRate = 5;
  open(writerObj);
end
%%
%i1=10;i2=105;
 G=graph(DkEps(squareform(pdist(zI')),.14));
 i1=10;i2=200;
% 
G=graph(DkEps(squareform(pdist(zI')),.15));
i1=3010 ;i2=2;

% G=graph(DkEps(squareform(pdist(zI')),.24));
% i1=2036;i2=2751;

% G=graph(DkEps(squareform(pdist(zI')),.2));
% i1=3010;i2=2466;

[PATH,D] = shortestpath(G,i1,i2);
length(PATH)
%
clf
plott(zMt,'.')
hold on
plot3(zI(1,PATH(1)),zI(2,PATH(1)),zI(3,PATH(1)),'g+','markersize',10,'linewidth',4)
plot3(zI(1,PATH(end)),zI(2,PATH(end)),zI(3,PATH(end)),'r+','markersize',10,'linewidth',4)
plott(zI(:,PATH),'.-','linewidth',4)
view(90,0)
%% interp z
ts=100;
newz=interp1((1:length(PATH))',zI(:,PATH)',(linspace(1,length(PATH),ts))')';
%%
%%
figure(1)
clf
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
subplot(223)
    plott(zMt,'k.')
    subplot(224)
    plott(zMt,'k.')
for i=1:length(newz)
    %%
    
    z=dlarray(newz(:,i),'CB');
    ps_p = dlarray(sigmoid(forward(decoderNet, z)),'BC');
    pcoded_reconstructed=extractdata(ps_p);
    s_rec=pcoded_reconstructed(size(M_coded,2)+1:end);
    p_reconstructed=net(pcoded_reconstructed(1:size(M_coded,2))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
    p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
     %%
     subplot(132)
     hold off
    plot_agent(p_reconstructed,s_rec,body,'m','k','linewidth',4);
        axis([-1.3,1.3,-.5,2])
        axis equal
        axis([-1.3,1.3,-.5,2])
        axis off
        %title(sprintf('path %d/%d',i,ts))
        title('Decoded posture and sensory inputs')
        set(gca,'fontsize',16)
        subplot(131)
        hold off
        scatter(zMt(1,:),zMt(2,:),1,'k.')
        hold on
        plot(newz(1,1:i),newz(2,1:i),'y.-','linewidth',2)
        scatter(newz(1,i),newz(2,i),50,'filled','k','markerfacecolor','r')
        axis square
        xlabel('z_1')
        ylabel('z_2')
        title('Latent space trajectory (z_1, z_2)')
        set(gca,'fontsize',16)
        subplot(133     )
        hold off
        scatter(zMt(3,:),zMt(2,:),1,'k.')
        hold on
        plot(newz(3,1:i),newz(2,1:i),'y.-','linewidth',2)
        scatter(newz(3,i),newz(2,i),50,'filled','k','markerfacecolor','r')
        axis square
        
        xlabel('z_3')
        ylabel('z_2')
        set(gca,'fontsize',16)
        title('Latent space trajectory (z_3, z_2)')
        drawnow
        if video
            frame=getframe(gcf);
            writeVideo(writerObj, frame);
        end
end
close(writerObj)

%% representative
%% latent trajectory

itouch=find(sum(S,2)>1);
MSt=[M_coded(itouch(1:end),:),S(itouch(1:end),:)];
MSt_dl=dlarray(single(MSt'),'CB');
[~, zmt, zLogvar] = sampling(encoderNet, MSt_dl);
zMt=extractdata(zmt);

%%
figure(1)
clf
cc=1;
scatter3(zMt(1,1:cc:end),zMt(2,1:cc:end),zMt(3,1:cc:end),10,'k.')
axis square
xlabel('z_1')
ylabel('z_2')
zlabel('z_3')
set(gca,'fontsize',16)
hold on
scatter3(zMt(1,4),zMt(2,4),zMt(3,4),400,'r','filled')
scatter3(zMt(1,7),zMt(2,7),zMt(3,7),400,'g','filled')
scatter3(zMt(1,24),zMt(2,24),zMt(3,24),400,'m','filled')
scatter3(zMt(1,20),zMt(2,20),zMt(3,20),400,'y','filled')
view(96,3)
%%
close all
figure(2)

i=7;
    clf
z=dlarray(zMt(:,i),'CB');
ps_p = dlarray(sigmoid(forward(decoderNet, z)),'BC');
pcoded_reconstructed=extractdata(ps_p);
s_rec=pcoded_reconstructed(size(M_coded,2)+1:end);
p_reconstructed=net(pcoded_reconstructed(1:size(M_coded,2))')'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;
p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better
%%hold off
plot_agent(p_reconstructed,s_rec,body,'m','k','linewidth',4);
axis([-1.3,1.3,-.5,2])
axis equal
axis([-1.3,1.3,-.5,2])
axis off
%title(sprintf('path %d/%d',i,ts))
set(gca,'fontsize',16)