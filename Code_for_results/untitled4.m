%%
p0=[0,0,0,0,0,0];
clf
subplot(311)
[Px,Py]=get_agent_limbs(p0,body);
decalage=sum(body.arm_lengths)/2;
plot(Px(2:end-1)+body.arm_lengths(1)-decalage,Py(2:end-1),'ko','markersize',10,'linewidth',3,'markerfacecolor','k')
hold on
plot_agent(p0,ones(1,size(S,2))*.4,body,'w','k','linewidth',6);

plot(.2-decalage,0,'r|','linewidth',4,'markersize',20)
plot(5.4-.2-decalage,0,'r|','linewidth',4,'markersize',20)
yticks([])
ylabel('')
xticks([0,.2,.5,1.3,2,3.4,4.1,4.9,5.2,5.4]-decalage)
xticklabels({'0','x_1','.5','1.3','2','3.4','4.1','4.9','x_2','L'})
set(gca,'color','none')
set(gca,'fontsize',18)
%axis off
%
x=linspace(0,5.4,1000);
s=[];
subplot(312)
for i=1:1000
s(i,:)=mannella_artificial_touch_continuous_position(x(i),body);
end

plot(x-decalage,s,'k-')
hold on
line(-decalage+[body.tactilesensors.means',body.tactilesensors.means']',[zeros(1,body.tactilesensors.nb)',ones(1,body.tactilesensors.nb)']','linestyle','-.','color',[.5,.5,.5]*.3,'linewidth',1)
yticks([0,1])
xticks([0,.2,.5,1.3,2,3.4,4.1,4.9,5.2,5.4]-decalage)
xticklabels({'0','x_1','.5','1.3','2','3.4','4.1','4.9','x_2','L'})
set(gca,'fontsize',18)
%
s1=max(mannella_artificial_touch_continuous_position(.2,body),mannella_artificial_touch_continuous_position(5.2,body));
subplot(313)

imagesc(s1)
xlabel('')
colormap(parula)
colorbar('Ticks',[0,1])
caxis([0,1])
xticks([1,linspace(5,100,20)])
yticks([])
axis([0.5,100.5,.5,1.5])
%axis([-5,101,0.5,1.5])
set(gca,'fontsize',18)