# 2d-agent-self-touch-vae
## Description
This repository propose a Matlab simulation for a 2D simple stick-wide tactile Agent able to generate multiple self-touches and create latent representation of its self-touch
interaction using VAE.

## Paper and citation

Citation: Marcel, V.; O'Regan, Kevin, J. & Hoffmann, M. (2022), Learning to reach to own body from spontaneous self-touch using a generative model, in 'IEEE International Conference on Development and Learning (ICDL)', pp. 328-335.[pdf](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9962186), [youtube video](https://www.youtube.com/watch?v=gg_qySTwOdc).

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
![2d tactile agent](/images/agent_double_touch_2d.png "2d tactile Agent")
 
## Installation
Everything work on Matlab R2021b.
Requirement: 
- Communication Toolbox (awgn() function for creating noise and corrupting data requires)
- neural_network_toolbox (for the VAE implementation)
- signal_blocks (part of DSP System Toolbox)

## Usage
Main usage is intented to be through cell computation within Matlab.
launching main.m lunch the generation of a simulation with preset model parameters, encoders sizes. It may take long to train the VAE.

## Support
Send message to Valentin Marcel: valentin.marcel@fel.cvut.cz

## Roadmap

- 2D Vision simulation is underway.
- 3D simulation using MIMo simulation in Mujoco.

## Contributing

## Authors and acknowledgment

## License

## Project status
Currently into cleaning status
