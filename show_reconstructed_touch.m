p0=[0.6223    1.7068    1.1631    0.2978    0.4697    1.7735]; %very
p0=body.motor.motor_constraints_min;


p=constmot(rand(1,body.nb_motors)*10,body.motor.motor_constraints_min,body.motor.motor_constraints_max);
p_hist=p;
% target position
x=4.9;
s_target0=zeros(1,size(S,2));
%


% control param
%
p_coded = compute_activation(p,body);
%
[Px,Py]=get_agent_limbs(p,body);
[sensory_real,Xtouch]=mannella_tactile_continuous(Px,Py,body);
ps_real=[p_coded,sensory_real];
% FROM TARGET SENSATION COMPUTE TARGET MS
s_target_real=max(s_target,sensory_real);

ps_target_current=[p_coded,s_target_real];

ps_target_current_dl=dlarray(ps_target_current','CB');

[z, zMean, zLogvar] = sampling_distance(encoderNet, gather(ps_target_current_dl),2);
zm=z;

ps_pred = sigmoid(forward(decoderNet, zm));

pcoded_reconstructed = extractdata(ps_pred)';
%
p_reconstructed=net(((pcoded_reconstructed(1:size(M_coded,2)))'))'.*((body.motor.motor_constraints_max-body.motor.motor_constraints_min))/2+(body.motor.motor_constraints_max+body.motor.motor_constraints_min)/2;

p_reconstructed=max(min(p_reconstructed,body.motor.motor_constraints_max),body.motor.motor_constraints_min); %saturation is better

%
s_reconstructed = pcoded_reconstructed(size(M_coded,2)+1:end);

figure(1)
clf
subplot(221)
plot_agent(p,s_target0,body,'m','k','linewidth',4);
axis equal
title('current position')
subplot(223)
plot_agent(p,s_reconstructed,body,'m','k','linewidth',4);
axis equal
title('current pos.: simulated touch')
figure(1)
subplot(222)
plot_agent(p_reconstructed,s_reconstructed,body,'m','k','linewidth',4);
hold on
axis equal
title('simulated self-touch')
subplot(224)
plot((s_reconstructed))
title('reconstructed touch')
